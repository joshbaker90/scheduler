<?php

add_shortcode('swp_admin_activities', 'swp_admin_activities');
function swp_admin_activities()
{
require_once('output/admin-activities.php');
}

add_shortcode('swp_activities', 'swp_activities');
function swp_activities()
{
require_once('output/activities.php');
}

add_shortcode('swp_activity_item', 'swp_activity_item');
function swp_activity_item()
{
require_once('output/activity_item.php');
}

add_shortcode('swp_schedules', 'swp_schedules');
function swp_schedules()
{
require_once('output/schedules.php');
}

add_shortcode('swp_sync', 'swp_sync');
function swp_sync()
{
require_once('output/sync.php');
}

add_shortcode('swp_test1', 'swp_test1');
function swp_test1()
{
echo "
<h3>I am a block</h3>
<p> please dont hurt me</p>
";
}

add_shortcode('swp_deb', 'swp_deb');
function swp_deb()
{
require_once('output/debtest.php');
}

add_shortcode('swp_test', 'swp_test');
function swp_test()
{
require_once('output/test.php');
}

add_shortcode('swp_posttest', 'swp_posttest');
function swp_posttest()
{
require_once('output/test.php');
}

function swp_tb_prefix()
{
global $wpdb;
$wp_prefix = $wpdb->prefix;
}

?>