<?php
/*
add_action( 'restrict_manage_posts', 'bmy_restrict_manage_posts' );
function bmy_restrict_manage_posts() {
    global $typenow;
    $taxonomy = $typenow.'_type';
    if( $typenow != "page" && $typenow != "post" ){
        $filters = array($taxonomy);
        foreach ($filters as $tax_slug) {
            $tax_obj = get_taxonomy($tax_slug);
            $tax_name = $tax_obj->labels->name;
            $terms = get_terms($tax_slug);
            echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
            echo "<option value=''>Show All $tax_name</option>";
            foreach ($terms as $term) { echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>'; }
            echo "</select>";
        }
    }
}
*/





// Function used to automatically create activities
/*function create_activities_page()
  {
   //post status and options
    $post = array(
          'comment_status' => 'open',
          'ping_status' =>  'closed' ,
          'post_date' => date('Y-m-d H:i:s'),
          'post_name' => 'activities',
          'post_status' => 'publish' ,
          'post_title' => 'Activities',
          'post_type' => 'page',
    );
    //insert page and save the id
    $newvalue = wp_insert_post( $post, false );
    //save the id in the database
    update_option( 'mrpage', $newvalue );
  }

 Activates function if plugin is activated
register_activation_hook( __FILE__, 'create_activities_page');



 add_post_type_support( 'attachment:audio', 'thumbnail' );
*/

//if post type exists check
/*if ( post_type_exists( 'yth_activities', $post_id ) )
	echo 'This is a not a blog post.  It is a programme!';
else
	echo 'This is not a programme. :(';
*/


/*
// Function used to automatically create Activities page.
function create_activities_pages()
  {
   //post status and options
    $post = array(
          'comment_status' => 'open',
          'ping_status' =>  'closed' ,
          'post_date' => date('Y-m-d H:i:s'),
          'post_name' => 'activities',
          'post_status' => 'publish' ,
          'post_title' => 'Activities',
          'post_type' => 'page',
    );
    //insert page and save the id
    $newvalue = wp_insert_post( $post, false );
    //save the id in the database
    update_option( 'mrpage', $newvalue );
  }

// // Activates function if plugin is activated
register_activation_hook( __FILE__, 'create_activities_pages');
*/





/*
function get_registered_post_types() {
    global $wp_post_types;

    return array_keys( $wp_post_types );
    echo "<br><br><br><br><br><br><br><br>";
    echo "i lvoe josh";
}
get_registered_post_types();
*/

//////////////////////////////////////////////////////////
///////////////////Register Custom Meta Box/////////////////////
////////////////////////////////////////////////////////

/*

//custom meta box with all field types
$prefix = 'dbt_';
$meta_box = array(
    'id' => 'my-meta-box',
    'title' => 'Custom meta box',
    'page' => 'activities',
    'context' => 'normal',
    'priority' => 'high',
    'fields' => array(
        array(
            'name' => 'Text box',
            'desc' => 'Enter something here',
            'id' => $prefix . 'text',
            'type' => 'text',
            'std' => 'Default value 1'
        ),
        array(
            'name' => 'Textarea',
            'desc' => 'Enter big text here',
            'id' => $prefix . 'textarea',
            'type' => 'textarea',
            'std' => 'Default value 2'
        ),
        array(
            'name' => 'Select box',
            'id' => $prefix . 'select',
            'type' => 'select',
            'options' => array('Option 1', 'Option 2', 'Option 3')
        ),
        array(
            'name' => 'Radio',
            'id' => $prefix . 'radio',
            'type' => 'radio',
            'options' => array(
                array('name' => 'Name 1', 'value' => 'Value 1'),
                array('name' => 'Name 2', 'value' => 'Value 2')
            )
        ),
        array(
            'name' => 'Checkbox',
            'id' => $prefix . 'checkbox',
            'type' => 'checkbox'
        )
    )
);

add_action('admin_menu', 'mytheme_add_box');
// Add meta box
function mytheme_add_box() {
    global $meta_box;
    add_meta_box($meta_box['id'], $meta_box['title'], 'mytheme_show_box', $meta_box['page'], $meta_box['context'], $meta_box['priority']);

}

// Callback function to show fields in meta box
function mytheme_show_box() {
    global $meta_box, $post;
    // Use nonce for verification
    echo '<input type="hidden" name="mytheme_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';
    echo '<table class="form-table">';
    foreach ($meta_box['fields'] as $field) {
        // get current post meta data
        $meta = get_post_meta($post->ID, $field['id'], true);
        echo '<tr>',
                '<th style="width:20%"><label for="', $field['id'], '">', $field['name'], '</label></th>',
                '<td>';
        switch ($field['type']) {
            case 'text':
                echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />', '<br />', $field['desc'];
                break;
            case 'textarea':
                echo '<textarea name="', $field['id'], '" id="', $field['id'], '" cols="60" rows="4" style="width:97%">', $meta ? $meta : $field['std'], '</textarea>', '<br />', $field['desc'];
                break;
            case 'select':
                echo '<select name="', $field['id'], '" id="', $field['id'], '">';
                foreach ($field['options'] as $option) {
                    echo '<option ', $meta == $option ? ' selected="selected"' : '', '>', $option, '</option>';
                }
                echo '</select>';
                break;
            case 'radio':
                foreach ($field['options'] as $option) {
                    echo '<input type="radio" name="', $field['id'], '" value="', $option['value'], '"', $meta == $option['value'] ? ' checked="checked"' : '', ' />', $option['name'];
                }
                break;
            case 'checkbox':
                echo '<input type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />';
                break;
        }
        echo     '</td><td>',
            '</td></tr>';
    }
    echo '</table>';
}


add_action('save_post', 'mytheme_save_data');
// Save data from meta box
function mytheme_save_data($post_id) {
    global $meta_box;
    // verify nonce
    if (!wp_verify_nonce($_POST['mytheme_meta_box_nonce'], basename(__FILE__))) {
        return $post_id;
    }
    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }
    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return $post_id;
        }
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }
    foreach ($meta_box['fields'] as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];
        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    }













//Activities custom field goal

//show meta box in post editing page
add_action('add_meta_boxes', 'goal_add_metabox');

//save meta box data
add_action('save_post', 'goal_save_metabox');

function goal_add_metabox() {
add_meta_box('goal', 'The Goal', 'goal_handler', 'activities'); 
}

//meta box handler
function goal_handler() {
  $value = get_post_custom($post->ID);
  $goal = esc_attr($value['goal'][0]);
  echo '<label for="goal"></label><br><input type="text" id="goal" name="goal" value="'.$goal.'" />';


}

//save metabox data
function goal_save_metabox($post_id){
  if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
  return; 
  }
  
  //check if current user can save post
  if( !current_user_can( 'edit_post' ) ) {
  return; 
  }
  
  if( isset($_POST['goal'] )) {
    update_post_meta($post_id, 'goal', $_POST['goal']); 
  }
}

//Activities custom field equipment

//show meta box in post editing page
add_action('add_meta_boxes', 'equipment_add_metabox');

//save meta box data
add_action('save_post', 'equipment_save_metabox');

function equipment_add_metabox() {
add_meta_box('equipment', 'Equipment Needed', 'equipment_handler', 'activities'); 
}

//meta box handler
function equipment_handler() {
  $value = get_post_custom($post->ID);
  $equipment = esc_attr($value['equipment'][0]);
  echo '<label for="equipment"></label><br><input type="text" id="equipment" name="equipment" value="'.$equipment.'" />';


}

//save metabox data
function equipment_save_metabox($post_id){
  if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
  return; 
  }
  
  //check if current user can save post
  if( !current_user_can( 'edit_post' ) ) {
  return; 
  }
  
  if( isset($_POST['equipment'] )) {
    update_post_meta($post_id, 'equipment', $_POST['equipment']); 
  }
}



//Activities custom field url_link

//show meta box in post editing page
add_action('add_meta_boxes', 'link_add_metabox');

//save meta box data
add_action('save_post', 'link_save_metabox');

function link_add_metabox() {
add_meta_box('link_url', 'Custom link/url', 'link_url_handler', 'activities');  
}

//meta box handler
function link_url_handler() {
  $value = get_post_custom($post->ID);
  $link_url = esc_attr($value['link_url'][0]);
  echo '<label for="link_url"></label><br><input type="text" id="link_url" name="link_url" value="'.$link_url.'" />';

}

//save metabox data
function link_save_metabox($post_id){
  if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
  return; 
  }
  
  //check if current user can save post
  if( !current_user_can( 'edit_post' ) ) {
  return; 
  }
  
  if( isset($_POST['link_url'] )) {
    update_post_meta($post_id, 'link_url', esc_url($_POST['link_url']));  
  }

}



//Activities custom field url_link

//show meta box in post editing page
add_action('add_meta_boxes', 'duration_add_metabox');

//save meta box data
add_action('save_post', 'duration_save_metabox');

function duration_add_metabox() {
add_meta_box('duration', 'Activity duration', 'duration_handler', 'activities', 'side', 'high');  
}

//meta box handler
function duration_handler() {
  $value = get_post_custom($post->ID);
  $duration = esc_attr($value['duration'][0]);
  echo '<label for="duration"><img src='.plugins_url('icons/chronometer10.png', __FILE__ ).'></label><input type="number" id="duration" name="duration" value="'.$duration.'" />';

}

//save metabox data
function duration_save_metabox($post_id){
  if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
  return; 
  }
  
  //check if current user can save post
  if( !current_user_can( 'edit_post' ) ) {
  return; 
  }
  
  if( isset($_POST['duration'] )) {
    update_post_meta($post_id, 'duration', $_POST['duration']); 
  }

}








//update the activity topic - THIS DOES NOT RECOUNT TOPICS THAT WERE DELETE FROM ITEMS. IT ONLY RECOUNTS THE NEW TOPIC IN THE TARGET ITEM
mysqli_query($con, "DELETE FROM wp_term_relationships WHERE object_ID= '$postid'");
foreach ($topicarray as $topictitle) {

$ttid = mysqli_query($con, "SELECT * FROM wp_terms WHERE name = '$topictitle'");
if(mysqli_num_rows($ttid) == 0) {
     // If topic isnt found...

$slug = str_replace(" ", "-", $topictitle);
mysqli_query($con, "INSERT INTO wp_terms (name, slug, term_group)
VALUES ('$topictitle', '$slug', '0' )");

$wp_termsid = mysqli_query($con, "SELECT * FROM wp_terms WHERE name = '$topictitle'");
foreach ($wp_termsid as $row) {
$termid = $row['term_id'];
}

mysqli_query($con,"INSERT INTO wp_term_relationships (object_ID, term_taxonomy_id)
VALUES ('$postid', '$termid')");

mysqli_query($con, "INSERT INTO wp_term_taxonomy (term_taxonomy_id, term_id, taxonomy, description, parent, count)
VALUES ('$termid', '$termid', 'topics', '0', '0', '1' )");

$tcount = mysqli_query($con, "SELECT * FROM wp_term_relationships WHERE term_taxonomy_id='$termid'");
$termcount = mysqli_num_rows($tcount);

mysqli_query($con,"UPDATE wp_term_taxonomy SET count='$termcount'
WHERE (term_id ='$termid')");

} else {
    // if topic is found...
foreach ($ttid as $row) {
  $termid = $row['term_id'];

mysqli_query($con,"INSERT INTO wp_term_relationships (object_ID, term_taxonomy_id)
VALUES ('$postid', '$termid')");

$tcount = mysqli_query($con, "SELECT * FROM wp_term_relationships WHERE term_taxonomy_id='$termid'");
$termcount = mysqli_num_rows($tcount);

mysqli_query($con,"UPDATE wp_term_taxonomy SET count='$termcount'
WHERE (term_id ='$termid')");
}

}
}
unset($topicarray);


//UPDATE COUNT topic count// - THIS RECOUNTS ALL TOPICS AT THE END OF THE SCRIPTS
$ttaxonomy = mysqli_query($con, "SELECT * FROM wp_term_taxonomy WHERE taxonomy='topics'");
foreach ($ttaxonomy as $ttax) {
$termid = $ttax['term_id'];

$tcount = mysqli_query($con, "SELECT * FROM wp_term_relationships WHERE term_taxonomy_id='$termid'");
$termcount = mysqli_num_rows($tcount);

mysqli_query($con,"UPDATE wp_term_taxonomy SET count='$termcount'
WHERE (term_id ='$termid')");
}


?>