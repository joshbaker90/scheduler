<html>
<?php
 get_header(); ?>


<div class="container-wrap">

<H2>Get posts list style</H2>
<?php wp_get_archives( array( 'type' => 'postbypost', 'limit' => 10, 'format' => 'html' ) ); ?>
<br><br>


<H2>Get posts via header</H2>
<ul>
  <?php $the_query = new WP_Query( 'showposts=5' ); ?>

  <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
  <li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>

  <li><?php echo substr(strip_tags($post->post_content), 0, 250);?></li>
  <?php endwhile;?>
</ul>

<H2>Get posts full detail</H2>
<br><br>
<ul>
<?php $the_query = new WP_Query( 'showposts=5' ); ?>
<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
<li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
<li><?php the_content(__('(more…)')); ?></li>
<?php endwhile;?>
</ul>


<H2>Get custom post types</H2>

        <div id="container">
            <div id="content">

<?php
$type = 'activities';
$args=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'caller_get_posts'=> 1);

$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
  while ($my_query->have_posts()) : $my_query->the_post(); ?>
    <p><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
    <?php the_content(__('(more…)')); ?></p>
    <?php
  endwhile;
}
wp_reset_query();  // Restore global post data stomped by the_post().
?>
            </div><!-- #content -->
        </div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>

</div>
-->
*/