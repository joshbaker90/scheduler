<?php 
require_once('shortcodes.php');
/*template name: Admin Activities*/
get_header();
?>


<div class="container-wrap">

<?php





$type = 'activities';
$args=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'caller_get_posts'=> 1);

$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
  while ($my_query->have_posts()) : $my_query->the_post(); ?>
    <p><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"></a></p>
    
<?php
//Get content
$title = get_the_title();
$topic = get_the_term_list( $post->ID, 'topics', '', ', ' );
$duration = get_post_meta(get_the_ID(), 'duration', true);
$description = get_the_content('more');
$goal = get_post_meta(get_the_ID(), 'goal', true);
$equipment = get_post_meta(get_the_ID(), 'equipment', true);
$link = get_post_meta(get_the_ID(), 'link_url', true);
$id = get_the_ID();
?>

<div class='content'>
<div class='inline'>

<div id="div1<?php echo "$id" ?>" class="targetDiv">
<p class="inline-block">Are you sure you want to delete this activity?</p>

<button class="delete-button-no showSingle float-right" target="1<?php echo "$id" ?>"><?php echo "<img src='".plugins_url('icons/delete99.png', __FILE__ )."'   style='width:16px;height:16px' alt='Delete'>"; ?></button>

<form class="inline-block float-right" action="<?php echo get_delete_post_link( $post->ID ) ?>" method="post">
<input name="unique_id" type="hidden" value="$id">
<input name="podio_id" type="hidden" value="$Podioid">
<input name="pagename" type="hidden" value="$pagename">
<button class="editactivity showSingle float-right" target="1<?php echo "$id" ?>"><?php echo "<img src='".plugins_url('icons/done.png', __FILE__ )."'   style='width:16px;height:16px' alt='Delete'>"; ?></button></form>

</div>

<div class="inline float-right">
<button class="delete-button-no showSingle inline float-right" target="1<?php echo "$id" ?>"><?php echo "<img src='".plugins_url('icons/delete81.png', __FILE__ )."'   style='width:16px;height:16px' alt='Delete'>"; ?></button>
</div>

<div class="inline activitylayer float-right">
<form class="inline-block float-right" action="wp-admin/post.php?post=<?php echo $id ?>&action=edit" method="post" target="_blank">
<button class='editactivity float-right' value='toggle'><?php echo "<img src='".plugins_url('icons/pencil112.png', __FILE__ )."'   style='width:16px;height:16px' alt='Edit'>"; ?></button></form>
</div>

<div class="inline float-right">

<button class="toggle showSingle inline float-right" target="<?php echo "$id" ?>">...</button>
</div>

<?php

//Activity duration
if(empty($duration))
{ }
else {
echo "<div class='inline'><div class='durationwrap'>
<span class='durationtext'>" . $duration . " minutes ". "</span>
<span class='durationimg'><img src='" . plugins_url( 'icons/chronometer10.png', __FILE__ ) . "' style='width:16px;height:16px' alt='Duration:'></span>
</div></div>";
}

//Activity topic
if(empty($topic))
{ }
else {
echo "<div class='inline float-right'><div class='topicwrap'>
<span class='topic'><b>Topic: </b>".$topic."</span>
</div></div>";
}

?>

</div>

<?php
//echo the content

//Activity title
if(empty($title))
{ 

}
else { 
echo "<div class='inline'><span class='activitytitle'><h3>".$title."</h3></span></div>";
}
?>




<!-- Activity goal -->
<?php
if(empty($goal))
{ 

}
else { 
echo "<br><div class='items'>
<span class='fieldtitle'><p><b>Goal:&nbsp;</b></p></span><span class='fieldcontent'>".$goal."</span></div>";
}
?>



<!-- Activity image
//  echo '<span>Image:'  . $row['image'] . "" . "</span>";  -->

<div class='items'>

</div>

<div id="div<?php echo "$id" ?>" class="targetDiv">


<?php

// Activity  description
if(empty($description))
{}
else { 
echo "<br><div class='items'>
<span class='fieldtitle'><p><b>Description:&nbsp;</b></p></span><span class='fieldcontent'>".$description."</span></div>";
}


// Activity Equipment
if(empty($equipment))
{}
else { 
echo "<br><div class='items'>
<span class='fieldtitle'><p><b>Equipment:&nbsp;</b></p></span><span class='fieldcontent'>".$equipment."</span></div>";
}


// Activity Link
if(empty($link))
{}
else { 
echo "<br><div class='items'>
<span class='fieldtitle'><p><b>Link:&nbsp;</b></p></span><span class='fieldcontent'><a href ='".$link."' target='blank'>".$link."</a></span></div>";
}
?>






<!-- echo "<br><div class=\"items\"><span class=\"fieldtitle\"><p><b>Link:&nbsp;</b></p></span><span class=\"fieldcontent\"><p>"  . $row['Link'] . "</p></span></div>";

echo "<div class=\"pinterest\"><a href=\"//www.pinterest.com/pin/create/button/\" data-pin-do=\"buttonBookmark\"  data-pin-color=\"red\" data-pin-height=\"28\"><img src=\"//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_red_28.png\" /></a>
<!-- Please call pinit.js only once per page -->
<!-- <script type=\"text/javascript\" async defer src=\"//assets.pinterest.com/js/pinit.js\"></script></div>";
-->

</div>
<br><br><hr></div>



<?php
  endwhile;
}
wp_reset_query();  // Restore global post data stomped by the_post().
?>
           
</div><!-- #content -->
</div><!-- #container -->
</div>
</div>
<div class="container main-content">
</div>

<?php get_footer(); ?>
