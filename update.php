{
    "name" : "Scheduler",
    "slug" : "swp_Scheduler",
    "download_url" : "https://joshbaker90@bitbucket.org/joshbaker90/scheduler.git",
    "version" : "2.0",
    "author" : "Joshua Baker",
    "sections" : {
        "description" : "Create schedules and a library of Activities to fill them with"
    }
}