
<div class="wrap">
<h2>Your Plugin Page Title</h2>
<form method="post" action="options.php">
<?php
settings_fields( 'myoption-group' );
do_settings_sections( 'myoption-group' );?>
<?php submit_button(); ?>
</form>
</div>
<?php
if ( is_admin() ){ // admin actions
  add_action( 'admin_menu', 'add_mymenu' );
  add_action( 'admin_init', 'register_mysettings' );
} else {
  // non-admin enqueues, actions, and filters
}
function register_mysettings() { // whitelist options
  register_setting( 'myoption-group', 'new_option_name' );
  register_setting( 'myoption-group', 'some_other_option' );
  register_setting( 'myoption-group', 'option_etc' );
}
?>