<?php

require_once(ABSPATH."wp-content/plugins/scheduler/podio/config.php");
require_once(ABSPATH."wp-content/plugins/scheduler/podio/PodioAPI.php");
require_once(ABSPATH."wp-content/plugins/scheduler/podio/authorise.php");

activityauth();
//  Podio::$debug = true;
if(empty($podioid)) {
  $podioid = 'a';
}


try {
PodioItem::update($podioid, array('fields' => $podioarray,
$options = array('hook' => false)
));
}
catch (PodioError $e) {
  // Something went wrong. Examine $e->body['error_description'] for a description of the error.
$error = $e->body['error_description'];
$error = $e->body['error'];
if ($error == 'not_found') {
        delete_post_meta($postid, $sprefix.'podioid', $podioid);
        require_once(ABSPATH."wp-content/plugins/scheduler/podiohooks/tempitem.php");
        require_once(ABSPATH."wp-content/plugins/scheduler/podiohooks/fielddata.php");
try {
PodioItem::update($podioid, array('fields' => $podioarray,
$options = array('hook' => false)
));
}
catch (PodioError $e) {
echo $e->body['error'];

}
if ($error ='PodioConnectionError') {
require_once(ABSPATH."wp-content/plugins/scheduler/podiohooks/update.php");  
}}
}



$lastupdatedby = get_post_meta($postid, $sprefix.'lastupdatedby', true);
if(empty($lastupdatedby)) {
add_post_meta($postid, $sprefix.'lastupdatedby', 'wordpress', true);
//echo "lastupdatedby added";
}
  else
{
update_post_meta($postid, $sprefix.'lastupdatedby', 'wordpress', true);
//echo " lastupdatedby update to podio";
}

global $wpdb;
$wpdb->update( "$wpprefix"."posts", array( 'post_name' => 'activity'), array( 'id' => $postid, 'post_type' => 'activities'), array( '%s' ) );


$item = PodioItem::get( $podioid );
if(isset($item->last_event_on))
{
$olastpodio = $item->last_event_on;
$podiodate = date_format($olastpodio, 'y-m-d H:i:s');
$podiodate = date('Y-m-d H:i:s',strtotime($podiodate." UTC"))."\n";
$pmd = $podiodate;
}

global $wpdb;
$wpdb->update( "$wpprefix"."posts", array( 'post_modified' => $pmd), array( 'ID' => $postid ), array( '%s' ) );
unset($podioid);