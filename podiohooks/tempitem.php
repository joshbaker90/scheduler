<?php

require_once(ABSPATH."wp-content/plugins/scheduler/podio/config.php");
require_once(ABSPATH."wp-content/plugins/scheduler/podio/PodioAPI.php");
require_once(ABSPATH."wp-content/plugins/scheduler/podio/authorise.php");

function randNum() { return abs(rand(100000000000000, 900000000000000)); }
$randno = randNum();
$post_id = "wp".$randno;

$sprefix = 'swp_';
$new = "I'm a new creation";
activityauth();

$podioarray['origin'] = $post_id;
$podioarray['wppostid'] = $postid;

PodioItem::create(8327586, array('fields' => $podioarray,
$options = array('hook' => false)));

unset($podioid);

add_post_meta($postid, $sprefix.'lastupdatedby', 'wordpress', true);
add_post_meta($postid, $sprefix.'origin', $post_id, true);

$newitemarray[] = array("id" => 111, "origin" => "dummy item item");

$item_array = PodioItem::filter(8327586, $attributes = array('limit' => 500) );
foreach ($item_array as $item) {
$id = $item->item_id;
if(isset($item->fields['origin']->values))
{ 
$oorigin = $item->fields['origin']->values;
$origin = strip_tags(str_replace("'","(apos)","$oorigin"));
}
else { $origin ="";
}
$newitemarray[] = array("id" => $id, "origin" => $origin);
}
$array = array_column($newitemarray, 'origin');
$key = array_search($post_id, $array);

$item_id = $newitemarray[$key]['id'];
$title = $newitemarray[$key]['origin'];

add_post_meta($postid, $sprefix.'podioid', $item_id, true);
$wppodioid = get_post_meta($postid, $sprefix.'podioid', true);
$podioid = $wppodioid;
unset($newitemarray);
?>