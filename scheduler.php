<?php
/**
* Plugin Name: Scheduler
* Plugin URI: http://churchpress.co.uk
* Description: A tool to help organise events
* Version: 1.4
* Author: Joshua Baker
* Author URI: http://churchpress.co.uk
**/
$swpplugin_url = plugins_url('/');

require 'plugin-updates/plugin-update-checker.php';
$ExampleUpdateChecker =PucFactory::buildUpdateChecker(
    'http://2.29.132.190/scheduler/update.json',
    __FILE__,
    'scheduler'
);

require_once('templates.php'); 
require_once 'shortcodes.php';

$swpplugin_url = plugins_url('/');

    /**
 * Register with hook 'wp_enqueue_scripts', which can be used for front end CSS and JavaScript
 */
add_action( 'wp_enqueue_scripts', 'prefix_add_my_stylesheet' );

/**
 * Enqueue plugin style-file
 */
function prefix_add_my_stylesheet() {
    // Respects SSL, Style.css is relative to the current file
    wp_register_style( 'prefix-style', plugins_url('css/style.css', __FILE__) );
    wp_enqueue_style( 'prefix-style' );
    
}

function wptuts_scripts_basic()
{
    // Register the script like this for a plugin:
    wp_register_script( 'custom-script', plugins_url( 'js/script.js', __FILE__ ) );
    // or
    // Register the script like this for a theme:
    //wp_register_script( 'custom-script', get_template_directory_uri() . 'js/script.js' );
 
    // For either a plugin or a theme, you can then enqueue the script:
    wp_enqueue_script( 'custom-script' );
}
add_action( 'wp_enqueue_scripts', 'wptuts_scripts_basic' );

function wptuts_scripts_with_jquery()
{
    // Register the script like this for a plugin:
    wp_register_script( 'custom-script', plugins_url( 'js/script.js', __FILE__ ), array( 'jquery' ) );
    // or
    // Register the script like this for a theme:
    //wp_register_script( 'custom-script', get_template_directory_uri() . 'js/script.js', array( 'jquery' ) );
 
    // For either a plugin or a theme, you can then enqueue the script:
    wp_enqueue_script( 'custom-script' );
}
add_action( 'wp_enqueue_scripts', 'wptuts_scripts_with_jquery' );

function schtest( $atts, $content = null)	{
 
	extract( shortcode_atts( array(
				'message' => 'ok then'
			), $atts 
		) 
	);
	// this will display our message before the content of the shortcode
	return $message . ' ' . $content;
	echo 'lol';
 
}
add_shortcode('example', 'schtest');


// Add columns to activities table
// GET FEATURED IMAGE
function yw_get_featured_image($post_ID) {
    $post_thumbnail_id = get_post_thumbnail_id($post_ID);
    if ($post_thumbnail_id) {
        $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'featured_preview');
        return $post_thumbnail_img[0];
    }
}

//yw get topics


// ADD NEW COLUMNS
function yw_columns_head($defaults) {
    $defaults['featured_image'] = 'Featured Image';
//    $defaults['topics_column'] = 'Topics';
 //   $defaults['groups_column'] = 'Groups';
    return $defaults;
}
 
// SHOW THE FEATURED IMAGE
function yw_columns_content($column_name, $post_ID) {
    if ($column_name == 'featured_image') {
        $post_featured_image = yw_get_featured_image($post_ID);
        if ($post_featured_image) {
            echo '<img src="' . $post_featured_image . '" />';
        }
        else {}
    }

    if ($column_name == 'topics_column') {
        $post_featured_image = yw_get_featured_image($post_ID);
        if ($post_featured_image) {
            echo '<img src="' . $post_featured_image . '" />';
        }
        else {}
    }

    if ($column_name == 'groups_column') {
        $post_featured_image = yw_get_featured_image($post_ID);
        if ($post_featured_image) {
            echo '<img src="' . $post_featured_image . '" />';
        }
        else {}
    }
}

// ONLY MOVIE CUSTOM TYPE POSTS
add_filter('manage_activities_posts_columns', 'yw_columns_head', 10);
add_action('manage_activities_posts_custom_column', 'yw_columns_content', 10, 2);


//////////////////////////////////////////////////////////
///////////////////REGISTER ACTIVITIES///////////////////
////////////////////////////////////////////////////////

// Register the Custom Activity Post Type
 function register_sch_activities() {
	    $labels = array(
        'name' => _x( 'Activities', 'activities' ),
        'singular_name' => _x( 'Activity', 'activities' ),
        'add_new' => _x( 'Add New activity', 'activities' ),
        'add_new_item' => _x( 'Add New Activity', 'activities' ),
        'edit_item' => _x( 'Edit Activity', 'activities' ),
        'new_item' => _x( 'New Activity', 'activities' ),
        'view_item' => _x( 'View Activity', 'activities' ),
        'search_items' => _x( 'Search Activities', 'activities' ),
        'not_found' => _x( 'No Activities', 'activities' ),
        'not_found_in_trash' => _x( 'No Activity found in Trash', 'activities' ),
        'parent_item_colon' => _x( 'Parent Activity:', 'activities' ),
        'menu_name' => _x( 'Activities', 'activities' ),
    );
 
    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'Activities filterable by topic',
//      'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'page-attributes' ),
        'supports' => array( 'title', 'editor', 'thumbnail', 'comments', 'revisions'),
        'taxonomies' => array( 'groups', 'category', 'topics' ),
        'public' => true,
        'show_ui' => true,
//      'show_in_menu' => true,
    	'show_in_menu' => 'edit.php?post_type=programmes',
        'menu_position' => 1,
  //    'menu_icon' => 'dashicons-format-audio',
  		'menu_icon' => 'http://localhost/wordpress/wp-content/plugins/scheduler-worker/icons/tennis8.png',
     //   'menu_icon' => 'plugins_url( "icons/chronometer10.png", __FILE__ )',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        
    );
 
    register_post_type( 'activities', $args );
    register_taxonomy_for_object_type( 'category', 'activities' );
}
 
add_action( 'init', 'register_sch_activities' );




//custom meta box with all field types
$prefix = 'swp_';
$meta_box = array(
    'id' => 'my-meta-box',
    'title' => 'Activity Details',
    'page' => 'activities',
    'context' => 'normal',
    'priority' => 'high',
    'fields' => array(
        array(
            'name' => 'Goal',
            'desc' => 'Summarise the general goal for the activity',
            'id' => $prefix . 'goal',
            'type' => 'text',
            'std' => ''
        ),
        array(
            'name' => 'Equipment needed',
            'desc' => 'What equipment is needed?',
            'id' => $prefix . 'equipment',
            'type' => 'textarea',
            'std' => ''
        ),
        array(
            'name' => 'Link/url',
            'desc' => 'any url or link',
            'id' => $prefix . 'link_url',
            'type' => 'text',
            'std' => ''
        ),
        array(
            'name' => 'Duration (Minutes)',
            'desc' => 'Duration',
            'id' => $prefix . 'duration',
            'type' => 'number',
            'std' => ''
        ),
        /*array(
            'name' => 'Select box',
            'id' => $prefix . 'select',
            'type' => 'select',
            'options' => array('Option 1', 'Option 2', 'Option 3')
        ),
        array(
            'name' => 'Radio',
            'id' => $prefix . 'radio',
            'type' => 'radio',
            'options' => array(
                array('name' => 'Name 1', 'value' => 'Value 1'),
                array('name' => 'Name 2', 'value' => 'Value 2')
            )
        ),
        array(
            'name' => 'Checkbox',
            'id' => $prefix . 'checkbox',
            'type' => 'checkbox'
        )*/
    )
);

add_action('admin_menu', 'mscheme_add_box');
// Add meta box
function mscheme_add_box() {
    global $meta_box;
    add_meta_box($meta_box['id'], $meta_box['title'], 'mscheme_show_box', $meta_box['page'], $meta_box['context'], $meta_box['priority']);

}

// Callback function to show fields in meta box
function mscheme_show_box() {
    global $meta_box, $post;
    // Use nonce for verification
    echo '<input type="hidden" name="mscheme_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';
    echo '<table class="form-table">';
    foreach ($meta_box['fields'] as $field) {
        // get current post meta data
        $meta = get_post_meta($post->ID, $field['id'], true);
        echo '<tr>',
                '<th style="width:20%"><label for="', $field['id'], '">', $field['name'], '</label></th>',
                '<td>';
        switch ($field['type']) {
            case 'text':
                echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />', '<br />', $field['desc'];
                break;
            case 'textarea':
                echo '<textarea name="', $field['id'], '" id="', $field['id'], '" cols="60" rows="4" style="width:97%">', $meta ? $meta : $field['std'], '</textarea>', '<br />', $field['desc'];
                break;
            case 'number':
                echo '<input type="number" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:10%" />', '<br />', $field['desc'];
                break;
            case 'select':
                echo '<select name="', $field['id'], '" id="', $field['id'], '">';
                foreach ($field['options'] as $option) {
                    echo '<option ', $meta == $option ? ' selected="selected"' : '', '>', $option, '</option>';
                }
                echo '</select>';
                break;
            case 'radio':
                foreach ($field['options'] as $option) {
                    echo '<input type="radio" name="', $field['id'], '" value="', $option['value'], '"', $meta == $option['value'] ? ' checked="checked"' : '', ' />', $option['name'];
                }
                break;
            case 'checkbox':
                echo '<input type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />';
                break;
        }
        echo     '</td><td>',
            '</td></tr>';
    }
    echo '</table>';
}


//add_action('save_post', 'mscheme_save_data');
// Save data from meta box
function mscheme_save_data($post_id) {
    global $meta_box, $post;
    global $meta_box;
    // verify nonce
    if (!isset($_POST["mscheme_meta_box_nonce"]) || !wp_verify_nonce($_POST["mscheme_meta_box_nonce"], basename(__FILE__))) {
        return $post_id;
    }

    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }
    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return $post_id;
        }
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }
    foreach ($meta_box['fields'] as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];
        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    }
}







//////////////////////////////////////////////////////////
///////////////////Register Programs/////////////////////
////////////////////////////////////////////////////////

// Register the Custom Programme Post Type
function register_sch_programme() {
	$labels = array(
        'name' => _x( 'Programmes', 'programmes' ),
        'singular_name' => _x( 'Programme', 'programmes' ),
        'add_new' => _x( 'Add New', 'programmes' ),
        'add_new_item' => _x( 'Add New Programme', 'programmes' ),
        'edit_item' => _x( 'Edit Programme', 'programmes' ),
        'new_item' => _x( 'New Programme', 'programmes' ),
        'view_item' => _x( 'View Programme', 'programmes' ),
        'search_items' => _x( 'Search Programmes', 'programmes' ),
        'not_found' => _x( 'No Programme', 'programmes' ),
        'not_found_in_trash' => _x( 'No Programmes found in Trash', 'programmes' ),
        'parent_item_colon' => _x( 'Parent Activity:', 'programmes' ),
        'menu_name' => _x( 'Programme', 'programmes' ),
    );
 
    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'programmes filterable by date',
//        'supports' => array( 'title', 'editor','thumbnail', 'comments', 'revisions'),
         'supports' => array( 'title', 'thumbnail', 'comments', 'revisions'),
        'taxonomies' => array( 'groups', 'category', 'topics' ),
        'public' => true,
        'show_ui' => true,
      	'show_in_menu' => true,
 // 	'show_in_menu' => 'edit.php?post_type=activities',
        'menu_position' => 6,
        'menu_icon' => 'dashicons-analytics',
 // 	'menu_icon' => 'http://localhost/wordpress/wp-content/plugins/scheduler%20Worker/icons/dj/dj1%20(1).png',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        );
 
    register_post_type( 'programmes', $args );
}
add_action( 'init', 'register_sch_programme' );






//custom meta box with all field types
$sprefix = 'swp_';
$swp_schedules_meta_box = array(
    'id' => 'my-meta-box',
    'title' => 'The Programme',
    'page' => 'programmes',
    'context' => 'normal',
    'priority' => 'high',
    'fields' => array(
        array(
            'name' => 'Goal',
            'desc' => 'Summarise the general goal for the activity',
            'id' => $sprefix . 'goal',
            'type' => 'text',
            'std' => ''
        ),
        array(
            'name' => 'Equipment needed',
            'desc' => 'What equipment is needed?',
            'id' => $sprefix . 'equipment',
            'type' => 'textarea',
            'std' => ''
        ),
        array(
            'name' => 'Link/url',
            'desc' => 'any url or link',
            'id' => $sprefix . 'link_url',
            'type' => 'text',
            'std' => ''
        ),
        array(
            'name' => 'Duration (Minutes)',
            'desc' => 'Duration',
            'id' => $sprefix . 'duration',
            'type' => 'number',
            'std' => ''
        ),
        /*array(
            'name' => 'Select box',
            'id' => $prefix . 'select',
            'type' => 'select',
            'options' => array('Option 1', 'Option 2', 'Option 3')
        ),
        array(
            'name' => 'Radio',
            'id' => $prefix . 'radio',
            'type' => 'radio',
            'options' => array(
                array('name' => 'Name 1', 'value' => 'Value 1'),
                array('name' => 'Name 2', 'value' => 'Value 2')
            )
        ),
        array(
            'name' => 'Checkbox',
            'id' => $prefix . 'checkbox',
            'type' => 'checkbox'
        )*/
    )
);






add_action('admin_menu', 'sch_schedule_add_box');
// Add meta box
function sch_schedule_add_box() {
    global $swp_schedules_meta_box;
    add_meta_box($swp_schedules_meta_box['id'], $swp_schedules_meta_box['title'], 'sch_schedule_show_box', $swp_schedules_meta_box['page'], $swp_schedules_meta_box['context'], $swp_schedules_meta_box['priority']);

}

// Callback function to show fields in meta box
function sch_schedule_show_box() {
    global $swp_schedules_meta_box, $post;
    // Use nonce for verification
    //echo '<input type="hidden" name="mscheme_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';
    //programme fields
    $swpplugin_url = plugins_url('/scheduler/');

//Get activities
$swp_postid = $post->ID;
$type = 'activities';
$args=array(
  'post_type' => $type,
  'posts_per_page' => -1,
  'ignore_sticky_posts'=> 1,
  'orderby'   => 'title',
  'order' => 'ASC',
    );

$my_query = null;
$my_query = new WP_Query($args);

if( $my_query->have_posts() ) {
      while ($my_query->have_posts()) : $my_query->the_post(); 

$sprefix= "swp_";

$title = get_the_title();
$topic = get_the_term_list($my_query->ID, 'topics', '', ', ' );
$duration = get_post_meta(get_the_ID(), $sprefix.'duration', true);
$description = get_the_content('more');
$goal = get_post_meta(get_the_ID(), $sprefix.'goal', true);
$equipment = get_post_meta(get_the_ID(), $sprefix.'equipment', true);
$link = get_post_meta(get_the_ID(), $sprefix.'link_url', true);
$id = get_the_ID();
$wpurl= site_url();

$activities[]= $id.' '.$title;

  endwhile;
}
wp_reset_query();

$args = array(
    'blog_id'      => $GLOBALS['blog_id'],
    'role'         => '',
    'meta_key'     => '',
    'meta_value'   => '',
    'meta_compare' => '',
    'meta_query'   => array(),
    'date_query'   => array(),        
    'include'      => array(),
    'exclude'      => array(),
    'orderby'      => 'login',
    'order'        => 'ASC',
    'offset'       => '',
    'search'       => '',
    'number'       => '',
    'count_total'  => false,
    'fields'       => 'all',
    'who'          => ''
 ); 
$swp_users = get_users( $args );
foreach($swp_users as $user){
$swp_userlist[] = $user->display_name;
}
    ?>

   <!-- BEGIN syntax highlighter -->
    <!-- <script type="text/javascript" src="<?php echo $swpplugin_url; ?>/sh/shCore.js"></script> -->
    <!-- <script type="text/javascript" src="<?php echo $swpplugin_url; ?>/sh/shBrushJScript.js"></script> -->
    <!-- <link type="text/css" rel="stylesheet" href="../wp-content/plugins/scheduler/sh/shCore.css"/> -->
    <!-- <link type="text/css" rel="stylesheet" href="../wp-content/plugins/scheduler/sh/shThemeDefault.css"/> -->
    <link rel="stylesheet" href="<?php echo $swpplugin_url; ?>/chosen/chosen.css">
    <link rel="stylesheet" href="<?php echo $swpplugin_url; ?>/css/scheduler.css">
    <script type="text/javascrsipt">
        SyntaxHighlighter.all();
    </script>
    <!-- END syntax highlighter -->
  
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> 
   <!--  <script type="text/javascript" src="http://localhost/wordpress/wp-content/plugins/scheduler/sh/jquery.searchabledropdown-1.0.8.min.js"></script> -->
   <!--<script type="text/javascript" src="<?php echo $swpplugin_url; ?>/sh/jquery.searchabledropdown-1.0.8.min.js"></script>-->

     <script src="<?php echo $swpplugin_url; ?>/js/jquery-1.10.2.js"></script>
                <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
                <script>
                var $wpsch = jQuery.noConflict();
                $wpsch(function() {
                $wpsch( "#sortable" ).sortable();
                $wpsch( "#sortable" ).disableSelection();
                }); </script>

                <script>
                function removeDiv(divId) {


                $wpsch("#"+divId).remove();
                //(counter = counter - 1);
                }

                var counter = 0;
                var limit = 200;
                function addInput(divName){
                     if (counter == limit)  {
                          alert("You have reached the limit of adding " + counter + " inputs");
                     }
                     else {
                          var newdiv = document.createElement('div'); newdiv.setAttribute("id", "Div"+counter); newdiv.setAttribute("class", "swp_frame");
                          newdiv.innerHTML = '<li class="ui-state-default"><div class="swp_scheduler_time">Time: <input type="time" id="swp_starttime" name="swp_schedule[' + (counter) + '][swp_starttime]" value="" >' 
                          +'  -  <input type="time" id="swp_endtime" name="swp_schedule[' + (counter) + '][swp_endtime]" value="" ></div>\u00A0' 
                          +'<div class="swp_scheduler_activity"><select data-placeholder="Choose an Activity..." id="swp_activity" class="chosen-select" name="swp_schedule['+ (counter) +'][swp_activity]" style="width:350px;" tabindex="2">'
                          +'<option value ="">Activity</option><?php if(isset($activities)){foreach ($activities as $activity) {
                            echo '<option value ="'.$activity.'"'; if (isset($oactivity)) { if ($oactivity == $activity) { echo "selected=selected";}} echo'>'.$activity.'</option>';
                            }}?></select></div>\u00A0'
                           +'<div class="swp_scheduler_leader"><select data-placeholder="Activity Leader.."  name="swp_schedule['+ (counter) +'][swp_person][]"  multiple class="chosen-select" style="width:350px;" tabindex="18" id="swp_person">'
                          +'<option value ="">...</option><?php if(isset($swp_userlist)){ foreach ($swp_userlist as $user) {
                            echo '<option value ="'.$user.'"';  echo'>'.$user.'</option>';
                            }} ?></select></div>\u00A0'
                          +'<div class="swp_scheduler_users"><select data-placeholder="other helpers.."  name="swp_schedule['+ (counter) +'][swp_persons][]"  multiple class="chosen-select" style="width:350px;" tabindex="18" id="swp_persons">' 
                          +'<option value ="">...</option><?php if(isset($swp_userlist)){ foreach ($swp_userlist as $users) {
                            echo '<option value ="'.$users.'"';  echo'>'.$users.'</option>';
                            }} ?></select><input type="hidden" name="swp_schedule['+ (counter) +'][swp_type]" value="activity"></div>\u00A0'
                          +'<button onclick="removeDiv(\'Div' + (counter) + '\');" id="swp_remove_button" class="button button-primary button-large">Remove</button></li>';
                          document.getElementById(divName).appendChild(newdiv);
                          counter++;
                        swp_multiselect()
                     }
                }

              </script>

              <h3>Programme Schedule</h3>
              <h4><i>Click and drag to sort schedule items</i></h4>

              <ul id="sortable">



<?php

$swpoldschedules = get_post_meta($swp_postid, $sprefix.'schedule', false);
$count = 0;

if(empty($swpoldschedules[0])){
echo '<div id="Div'.$count.'" class="swp_frame"><li class="ui-state-default">
<div class="swp_scheduler_time">Time: <input type="time" id="swp_starttime" name="swp_schedule['.$count.'][swp_starttime]" value="'.$ostarttime.'" >
 -  <input type="time" id="swp_endtime" name="swp_schedule['.$count.'][swp_endtime]" value="'.$oendtime.'" ></div>
<div class="swp_scheduler_activity"><select data-placeholder="Choose an Activity..." id="swp_activity" class="chosen-select" name="swp_schedule['.$count.'][swp_activity]" style="width:350px;" tabindex="2">
<option value ="">Activity</option>';
if(isset($activities)){foreach ($activities as $activity) {
echo '<option value ="'.$activity.'"'; if (isset($oactivity)) { if ($oactivity == $activity) { echo "selected=selected";}} echo'>'.$activity.'</option>';
}}
echo '</select></div>
<div class="swp_scheduler_leader"><select data-placeholder="Activity Leader.."  name="swp_schedule['.$count.'][swp_person][]"  multiple class="chosen-select" style="width:350px;" tabindex="18" id="swp_person">
<option value ="">...</option>';
if(isset($swp_userlist)){foreach ($swp_userlist as $user) {
echo '<option value ="'.$user.'"';if (isset($oaleaders)) {if (in_array($user, $oaleaders)) { echo "selected=selected";}} echo'>'.$user.'</option>';
}}
echo '</select></div>
<div class="swp_scheduler_users"><select data-placeholder="other helpers.."  name="swp_schedule['.$count.'][swp_persons][]"  multiple class="chosen-select" style="width:350px;" tabindex="18" id="swp_persons">
<option value ="">...</option>';
if(isset($swp_userlist)){foreach ($swp_userlist as $users) {
echo '<option value ="'.$users.'"';if (isset($opersons)) {if (in_array($users, $opersons)) { echo "selected=selected";}} echo'>'.$users.'</option>';
}}
echo '</select><input type="hidden" name="swp_schedule['.$count.'][swp_type]" value="activity"></div>
<button onclick="removeDiv(\'Div'.$count.'\');" id="swp_remove_button" class="button button-primary button-large">Remove</button>
</li></div>';
?><script type="text/javascript">counter++;</script>
<?php $count++;
}
else {

$swpoldschedule = unserialize(urldecode($swpoldschedules[0]));
foreach ($swpoldschedule as $item){
switch($item['swp_type']){
    case activity:
        if(isset($item['swp_starttime'])){
$ostarttime = $item['swp_starttime'];  
}
else { $ostarttime = ""; }

if(isset($item['swp_endtime'])){
$oendtime = $item['swp_endtime'];  
}
else { $oendtime = ""; }

if(isset($item['swp_activity'])){
$oactivity = $item['swp_activity']; 
}
else { $oactivity = ""; }

if(isset($item['swp_person'])){
$oaleaders = $item['swp_person'];  
}
else { $oaleaders = []; }

if(isset($item['swp_persons'])){
$opersons = $item['swp_persons'];  
}
else { $opersons = []; }

echo '<div id="Div'.$count.'" class="swp_frame"><li class="ui-state-default">
<div class="swp_scheduler_time">Time: <input type="time" id="swp_starttime" name="swp_schedule['.$count.'][swp_starttime]" value="'.$ostarttime.'" >
 - <input type="time" id="swp_endtime" name="swp_schedule['.$count.'][swp_endtime]" value="'.$oendtime.'"></div>
<div class="swp_scheduler_activity"><select data-placeholder="Choose an Activity..." id="swp_activity" class="chosen-select" name="swp_schedule['.$count.'][swp_activity]" style="width:350px;" tabindex="2">
<option value ="">Activity</option>';
if(isset($activities)){foreach ($activities as $activity) {
echo '<option value ="'.$activity.'"'; if (isset($oactivity)) { if ($oactivity == $activity) { echo "selected=selected";}} echo'>'.$activity.'</option>';
}}
echo '</select></div>
<div class="swp_scheduler_leader"><select data-placeholder="Activity Leader.." name="swp_schedule['.$count.'][swp_person][]" multiple class="chosen-select" style="width:350px;" tabindex="18" id="swp_person">
<option value ="">...</option>';
if(isset($swp_userlist)){foreach ($swp_userlist as $user) {
echo '<option value ="'.$user.'"';if (isset($oaleaders)) {if (in_array($user, $oaleaders)) { echo 'selected=selected';}} echo'>'.$user.'</option>';
}}
echo '</select></div>
<div class="swp_scheduler_users"><select data-placeholder="other helpers.." name="swp_schedule['.$count.'][swp_persons][]" multiple class="chosen-select" style="width:350px;" tabindex="18" id="swp_persons">
<option value ="">...</option>';
if(isset($swp_userlist)){foreach ($swp_userlist as $users) {
echo '<option value ="'.$users.'"';if (isset($opersons)) {if (in_array($users, $opersons)) { echo 'selected=selected';}} echo'>'.$users.'</option>';
}}
echo '</select><input type="hidden" name="swp_schedule['.$count.'][swp_type]" value="activity"></div>
<button onclick="removeDiv(\'Div'.$count.'\');" id="swp_remove_button" class="button button-primary button-large">Remove</button>
</li></div>';
?><script type="text/javascript">counter++;</script>
<?php $count++;

        break;
    case role:
        if(isset($item['swp_starttime'])){
$ostarttime = $item['swp_starttime'];  
}
else { $ostarttime = ""; }

if(isset($item['swp_endtime'])){
$oendtime = $item['swp_endtime'];  
}
else { $oendtime = ""; }

if(isset($item['swp_role'])){
$role = $item['swp_role']; 
}
else { $role = ""; }

if(isset($item['swp_person'])){
$oaleaders = $item['swp_person'];  
}
else { $oaleaders = []; }

if(isset($item['swp_persons'])){
$opersons =$item['swp_persons'];  
}
else { $opersons = [];}


echo '<div id="Div'.$count.'" class="swp_frame"><li class="ui-state-default">
<div class="swp_scheduler_time">Time: <input type="time" id="swp_starttime" name="swp_schedule['.$count.'][swp_starttime]" value="'.$ostarttime.'" >
 - <input type="time" id="swp_endtime" name="swp_schedule['.$count.'][swp_endtime]" value="'.$oendtime.'"></div>
<input placeholder="Role..."  type="text" id="swp_role" name="swp_schedule['.$count.'][swp_role]" value="'.$role.'" >
<div class="swp_scheduler_leader"><select data-placeholder="Role Leader.."  name="swp_schedule['.$count.'][swp_person][]"  multiple class="chosen-select" style="width:350px;" tabindex="18" id="swp_person">
<option value ="">...</option>';
if(isset($swp_userlist)){foreach ($swp_userlist as $user) {
echo '<option value ="'.$user.'"';if (isset($oaleaders)) {if (in_array($user, $oaleaders)) { echo "selected=selected";}} echo'>'.$user.'</option>';
}}
echo '</select></div>
<div class="swp_scheduler_users"><select data-placeholder="other helpers.."  name="swp_schedule['.$count.'][swp_persons][]"  multiple class="chosen-select" style="width:350px;" tabindex="18" id="swp_persons">
<option value ="">...</option>';
if(isset($swp_userlist)){foreach ($swp_userlist as $users) {
echo '<option value ="'.$users.'"';if (isset($opersons)) {if (in_array($users, $opersons)) { echo "selected=selected";}} echo'>'.$users.'</option>';
}}
echo '</select><input type="hidden" name="swp_schedule['.$count.'][swp_type]" value="role"></div>
<button onclick="removeDiv(\'Div'.$count.'\');" id="swp_remove_button" class="button button-primary button-large">Remove</button>
</li></div>';
?><script type="text/javascript">counter++;</script>
<?php $count++;

        break;
}

}}

?>
</ul>
<a id="topics-add-toggle" class="hide-if-no-js taxonomy-add-new swp_add" style="cursor:pointer;" onClick="addInput('sortable');"><u>+ Add Activity to the schedule</u></a>
<a id="topics-add-toggle" class="hide-if-no-js taxonomy-add-new swp_add" style="cursor:pointer;" onClick="swprole_addInput('sortable');"><u>+ Add Role to the schedule</u></a> 

          <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css"> -->

<script>
                function removeDiv(divId) {
                $("#"+divId).remove();
                //(counter = counter - 1);
                }

                //var rolecounter = 0;
                var limit = 200;
                function swprole_addInput(divName){
                     if (counter == limit)  {
                          alert("You have reached the limit of adding " + counter + " inputs");
                     }
                     else {
                          var newdiv = document.createElement('div'); newdiv.setAttribute("id", "Div"+counter); newdiv.setAttribute("class", "swp_frame");
                          newdiv.innerHTML = '<li class="ui-state-default"><div class="swp_scheduler_time">Time: <input type="time" id="swp_starttime" name="swp_schedule[' + (counter) + '][swp_starttime]" value="" >' 
                          +'  -  <input type="time" id="swp_endtime" name="swp_schedule[' + (counter) + '][swp_endtime]" value="" ></div>\u00A0'
                          +'<input placeholder="Role..."  type="text" id="swp_role" name="swp_schedule[' + (counter) + '][swp_role]" value="" >\u00A0' 
                          +'<div class="swp_scheduler_leader"><select data-placeholder="Role Leader.."  name="swp_schedule['+ (counter) +'][swp_person][]"  multiple class="chosen-select" style="width:350px;" tabindex="18" id="swp_person">'
                          +' <option value ="">...</option><?php if(isset($swp_userlist)){ foreach ($swp_userlist as $user) {
                            echo '<option value ="'.$user.'"';  echo'>'.$user.'</option>';
                            }} ?></select></div>\u00A0'
                          +'<div class="swp_scheduler_users"><select data-placeholder="other helpers.."  name="swp_schedule['+ (counter) +'][swp_persons][]"  multiple class="chosen-select" style="width:350px;" tabindex="18" id="swp_persons">' 
                          +' <option value ="">...</option><?php if(isset($swp_userlist)){ foreach ($swp_userlist as $users) {
                            echo '<option value ="'.$users.'"';  echo'>'.$users.'</option>';
                            }} ?></select><input type="hidden" name="swp_schedule['+ (counter) +'][swp_type]" value="role"></div>\u00A0'
                          +' <button onclick="removeDiv(\'Div' + (counter) + '\');" id="swp_remove_button" class="button button-primary button-large">Remove</button></li>';
                          document.getElementById(divName).appendChild(newdiv);
                          counter++;
                          swp_multiselect()
                     }
                }



              </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo $swpplugin_url; ?>/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo $swpplugin_url; ?>/chosen/js/add.js"></script>
    <script type="text/javascript">
    swp_multiselect()
    </script>

<?php

}


add_action('save_post', 'sch_schedule_save_data');
// Save data from meta box


function sch_schedule_save_data($post_id) {
$swpoldschedules = get_post_meta($post_id, $sprefix.'schedule', false);
print_r($_POST['swp_schedule']);
$sprefix = 'swp_';
if(isset($_POST['swp_schedule'])){
//print_r($_POST);
$swpschedules = $_POST['swp_schedule'];
}
if(isset($swpschedules)){
$serialize = serialize($swpschedules);  
$serialize = urlencode($serialize);
$post_id = $post_id;
$meta_key = $sprefix.'schedule';
$meta_value = $serialize;
//$prev_value = $swpoldschedules1;
update_post_meta($post_id, $meta_key, $meta_value /*$prev_value*/);
}
else {
$post_id = $post_id;
$meta_key = $sprefix.'schedule';
$meta_value = '';
update_post_meta($post_id, $meta_key, $meta_value /*$prev_value*/);
}
echo $serialize;
//udate_post_meta($post_id, $meta_key, $meta_value /*$prev_value*/);
if(isset($_POST['swp_roles'])){
$swprole = $_POST['swp_roles'];
}
if(isset($swprole)){
$serialize = serialize($swprole);  
$serialize = urlencode($serialize);
$post_id = $post_id;
$meta_key = $sprefix.'roles';
$meta_value = $serialize;
//$prev_value = $swpoldschedules1;
update_post_meta($post_id, $meta_key, $meta_value /*$prev_value*/);
}
else {
$post_id = $post_id;
$meta_key = $sprefix.'roles';
$meta_value = '';
update_post_meta($post_id, $meta_key, $meta_value /*$prev_value*/);
}
}



//////////////////////////////////////////////////////////
///////////////////REGISTER TAXONOMIES///////////////////
////////////////////////////////////////////////////////

//topic taxonomy
function topics_taxonomy() {
    register_taxonomy(
        'topics',
    	array( 'activities','programmes' ),
        array(
            'hierarchical' => true,
            'label' => 'Topics',
            'query_var' => true,
            'show_admin_column' => true,
            'update_count_callback' => '_update_post_term_count',
            'rewrite' => array(
                'slug' => 'topics',
                'with_front' => false
            )
        )
    );
}
add_action( 'init', 'topics_taxonomy');

//groups taxonomoy
function groups_taxonomy() {
    register_taxonomy(
        'groups',
        array( 'activities','programmes' ),
        array(
            'hierarchical' => true,
            'label' => 'Groups',
            'query_var' => true,
            'show_admin_column' => true,
            'update_count_callback' => '_update_post_term_count',
                'rewrite' => array(
                'slug' => 'groups',
                'with_front' => false,

                
            )
        )
    );
}
add_action('init', 'groups_taxonomy');


//custom Filters
add_action('restrict_manage_posts','yw_topics_filter');

        function yw_topics_filter() {
            global $typenow;

            if ($typenow=='activities'){
                         $args = array(
                             'show_option_all' => 'All Topics',
                             'taxonomy'        => 'topics',
                             'name'            => 'topics'

                         );
                wp_dropdown_categories($args);
                        }
        }
add_action( 'request', 'yw_topic_request' );
function yw_topic_request($request) {
    if (is_admin() && $GLOBALS['PHP_SELF'] == '/wp-admin/edit.php' && isset($request['post_type']) && $request['post_type']=='topics') {
        $request['term'] = get_term($request['topics'],'topics')->name;

    }
    return $request;
}

add_action('restrict_manage_posts','yw_groups_filter');

        function yw_groups_filter() {
            global $typenow;

            if ($typenow=='activities'){
                         $args = array(
                             'show_option_all' => 'All Groups',
                             'taxonomy'        => 'groups',
                             'name'            => 'groups',
                             'count'           => 1
                         );
                wp_dropdown_categories($args);
                        }
        }
add_action( 'request', 'yw_group_request' );
function yw_group_request($request) {
    if (is_admin() && $GLOBALS['PHP_SELF'] == '/wp-admin/edit.php' && isset($request['post_type']) && $request['post_type']=='groups') {
        $request['term'] = get_term($request['groups'],'groups')->name;

    }
    return $request;
}


function sch_admin_tabs( $current = 'homepage' ) {
    $tabs = array( 'homepage' => 'Home Settings', 'general' => 'General', 'footer' => 'Footer' );
    echo '<div id="icon-themes" class="icon32"><br></div>';
    echo '<h2 class="nav-tab-wrapper">';
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? ' nav-tab-active' : '';
        echo "<a class='nav-tab$class' href='?page=theme-settings&tab=$tab'>$name</a>";

    }
    echo '</h2>';
}



//////////////////////////////////////////////////////////
///////////////////SETTINGS PAGE/////////////////////////
////////////////////////////////////////////////////////
add_action('admin_menu', 'sch_work_settings');
 
function sch_work_settings() {
 
    add_options_page('Scheduler Settings', 'Scheduler Settings', 'administrator', 'scheduler_settings', 'scheduler_settings');
 
}


//settings page content
function scheduler_settings() {
 
    $slide_effect = (get_option('fwds_effect') == 'slide') ? 'selected' : '';
 
    $fade_effect = (get_option('fwds_effect') == 'fade') ? 'selected' : '';
 
    $interval = (get_option('fwds_interval') != '') ? get_option('fwds_interval') : '2000';
 
    $autoplay  = (get_option('fwds_autoplay') == 'enabled') ? 'checked' : '' ;
 
    $playBtn  = (get_option('fwds_playBtn') == 'enabled') ? 'checked' : '' ;

    global $wpdb;
    $swp_prefix = $wpdb->prefix;

    $hookurl ='/wp-content/plugins/scheduler/webhooks/hookregister.php';
    $hookdurl ='/wp-content/plugins/scheduler/webhooks/hookdelete.php';

    $args=array(

    );

$my_query = new WP_Query($args);
$wpurl= site_url();

if (preg_match('/localhost/',$wpurl)) {
$url1 = str_replace('localhost', '127.0.0.1', $wpurl);
//str_replace('low', 'high', $oldurl);
$hook_url = $url1 . $hookurl;
$hookd_url = $url1 . $hookdurl;
}
else {
$hook_url = $url . $hookurl;
$hookd_url = $url . $hookdurl;
}
wp_reset_query();  // Restore global post data stomped by the_post().


if (isset($_POST['r-hook'])){
//this is 'caller.php'
//should it come with full url?
$Url      = $hook_url;
$Handle   = curl_init($Url);
$Response = curl_exec($Handle);
curl_close($Handle);

//echo $Response;
}

if (isset($_POST['d-hook'])){
//this is 'caller.php'
//should it come with full url?
$Url      = $hookd_url;
$Handle   = curl_init($Url);
$Response = curl_exec($Handle);
curl_close($Handle);

//echo $Response;
}

if (isset($_POST['sync_a'])){
//this is 'caller.php'
//should it come with full url?
$Url      = plugins_url('output/ssync.php', __FILE__ );
$Handle   = curl_init($Url);
$Response = curl_exec($Handle);
curl_close($Handle);

//echo $Response;
}


$swpplugin_url = plugins_url('/scheduler/');

$html = '
</pre>
<div class="wrap">

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Case</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="'.plugins_url("css/bootstrap.css", __FILE__).'">
  <script src="'.$swpplugin_url.'/js/jquery.min.js"></script>
  <script src="'.$swpplugin_url.'/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Scheduler settings</h2>
  <ul class="nav nav-pills">
    <li class="active"><a data-toggle="pill" href="#home">General Settings</a></li>
    <li><a data-toggle="pill" href="#menu1">Program Settings</a></li>
    <li><a data-toggle="pill" href="#menu2">Activity settings</a></li>
    <li><a data-toggle="pill" href="#menu3">Localhost</a></li>
  </ul>
  
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
        <form action="options.php" method="post" name="options">
        ' . wp_nonce_field('update-options') . '
        <table class="form-table" width="100%" cellpadding="10">
        <tbody>
        <tr>
        <td scope="row" align="left">
         <label>Slider Effect </label>
        <select name="fwds_effect"><option value="slide">Slide</option><option value="fade">Fade</option></select></td>
        </tr>
        <tr>
        <td scope="row" align="left">
         <label>Enable Auto Play </label><input type="checkbox" name="fwds_autoplay" value="enabled" /></td>
        </tr>
        <tr>
        <td scope="row" align="left"><label>Enable Play Button </label><input type="checkbox" name="fwds_playBtn" value="enabled" /></td>
        </tr>
        <tr>
        <td scope="row" align="left">
         <label>Transition Interval</label><input type="text" name="fwds_interval" value="' . $interval . '" /></td>
        </tr>
        </tbody>
        </table>
 <input type="hidden" name="action" value="update" />
 
 <input type="hidden" name="page_options" value="fwds_autoplay,fwds_effect,fwds_interval,fwds_playBtn" />
 
 <input type="submit" name="Submit" value="Update" /></form>
 </div>



    <div id="menu1" class="tab-pane fade">
      <h3>Menu 1</h3>
      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div>



    <div id="menu2" class="tab-pane fade">
      <form method="post">
    <input type="submit" name="r-hook" value="Register hooks">
    </form>

    <form method="post">
    <input type="submit" name="d-hook" value="delete hooks">
    </form>

    <form method="post">
    <input type="submit" name="sync_a" value="Sync Activities">
    </form>
    </div>



    <div id="menu3" class="tab-pane fade">
      <h3></h3>
        <table class="form-table" width="100%" cellpadding="10">
        <tbody>
        <tr>
        <td scope="row" align="left">
         <label><b>Are you are running wordpress on local machine?</b></label>'." ".'<input type="checkbox" name="localhost" value="enabled" /></td>
        </tr>
        <tr>
         <td scope="row" align="left">
         <label>If your ip is not 127.0.0.1 state here</label>'." ".'<input type="text" name="localhost" value="127.0.0.1" /></td>
        </tr>
        <tr>
         <td scope="row" align="left">
         <label>Your wordpress table prefix is: </label>'." ".$swp_prefix.'</td>
        </tr>
        </table>
    </div>
  </div>
</div>




<!--<pre>--->
';
 
    echo $html;
if(isset($Response)) {    
    echo $Response;
}

}


//////////////////////////////////////////////////////////
///////////////CHANGE SUBMENU ORDER//////////////////////
////////////////////////////////////////////////////////
add_filter( 'custom_menu_order', 'submenu_order' );

function submenu_order( $menu_ord ) 
{
    global $submenu;
    unset( $submenu['edit.php?post_type=programmes'][10] );
    // Enable the next line to see all menu orders
    //echo '<pre>'.print_r($submenu,true).'</pre>';

    $arr = array();
    $arr[] = $submenu['edit.php?post_type=programmes'][5];     //my original order was 5,10,15,16,17,18
    $arr[] = $submenu['edit.php?post_type=programmes'][18];
//    $arr[] = $submenu['edit.php?post_type=programmes'][10];
    $arr[] = $submenu['edit.php?post_type=programmes'][17];
    $arr[] = $submenu['edit.php?post_type=programmes'][15];
    $arr[] = $submenu['edit.php?post_type=programmes'][16];
    $submenu['edit.php?post_type=programmes'] = $arr;

    return $menu_ord;
}

add_action( 'transition_post_status', 'swp_transition_post_status', 10, 3 );

function swp_transition_post_status( $new_status, $old_status, $post)
{

unset($update);
$sprefix = 'swp_';
$postid = $post->ID;
mscheme_save_data($postid);
$postname = $post->post_name;

    global $wpdb;
    $wpprefix = $wpdb->prefix;

    $lastupdatedby = get_post_meta($postid, "$sprefix"."lastupdatedby", true);
    if($postname === 'newpodioactivity') {
    $update = "no";
    update_post_meta($postid, "$sprefix"."lastupdatedby", "wordpress", $lastupdatedby);
    global $wpdb;
    $wpdb->update( "$wpprefix"."posts", array( 'post_name' => 'activity'), array( 'id' => $postid, 'post_type' => 'activities'), array( '%s' ) );
    }

elseif ($postname === 'updatepodioactivity') {
    $update = "no";
    update_post_meta($postid, "$sprefix"."lastupdatedby", "wordpress", $lastupdatedby);
    global $wpdb;
    $wpdb->update( "$wpprefix"."posts", array( 'post_name' => 'activity'), array( 'id' => $postid, 'post_type' => 'activities'), array( '%s' ) );
    }

else { $update = "yes";}
add_post_meta($postid, "$sprefix".'update', "$update", true);


if($update == 'yes') {
//add_post_meta($postid, "$sprefix".'update1', $update, false);

   if ($post->post_type == 'activities') {

    if ( 'auto-draft' == $new_status && 'auto-draft' !== $old_status ) {}

    if ('publish' == $new_status || 'draft' == $new_status || 'pending' == $new_status || 'future' == $new_status || 'private' == $new_status) {
        $sprefix = 'swp_';
        $publish = 'yes';
        $postid = $post->ID;
        

require_once(ABSPATH.'wp-content/plugins/scheduler/podiohooks/hook.php');

    }

    if ('trash' == $new_status){
    $sprefix = 'swp_';

        $wppodioid = get_post_meta(get_the_ID(), "$sprefix"."podioid", true);
     if(empty($wppodioid))
        {  
        }
        else { 
        $wppodioid = get_post_meta(get_the_ID(), "$sprefix"."podioid", true);
        $podioid = $wppodioid;
        require_once(ABSPATH.'wp-content/plugins/scheduler/podiohooks/delete.php');
        }
    }

}
unset($update);
}}


//////////////////////////////////////////////////////////
///////////////Create table on ACTIVATION//////////////////////
////////////////////////////////////////////////////////
global $table_version;
$table_version = "1.0";

function func_table_contactus(){
    creat_table_contactus();
}

function creat_table_contactus(){
    global $wpdb;
    global $table_version;

    $ssmanager = $wpdb->prefix."scheduler_sync-manager";
    $slider_ver = get_option("table_version");

    $check_table_contactus = $wpdb->get_var("SHOW TABLES LIKE '$ssmanager'");

    if($check_table_contactus != $ssmanager ||  $slider_ver != $ssmanager ) {
         $sql= "CREATE TABLE" .$ssmanager. "(
                   id INT(6) UNSIGNED NOT NULL AUTO_INCREMENT, 
                   postid VARCHAR(30) NOT NULL,
                   podioid VARCHAR(30) NOT NULL,
                   sync_type VARCHAR(30) NOT NULL,
                   status VARCHAR(30) NOT NULL,
           PRIMARY KEY (id)
               )";

    require_once(ABSPATH.'wp-admin/includes/upgrade.php');
        dbDelta($sql);

    update_option("table_version", $table_version);
    }
    else{
        add_option("table_version", $table_version);
    }

}
register_activation_hook( __FILE__, 'creat_table_contactus' );

//////////////////////////////////////////////////////////
///////////////ON PLUGIN DE-ACTIVATION///////////////////
////////////////////////////////////////////////////////
function my_plugin_deactivate() {

      require_once(ABSPATH.'wp-content/plugins/scheduler/webhooks/deletehook.php');
}
register_deactivation_hook( __FILE__, 'my_plugin_deactivate' );

?>