<script language="JavaScript">
<!--
// Use this function to retrieve a cookie.
function getCookie(name){
var cname = name + "=";               
var dc = document.cookie;             
    if (dc.length > 0) {              
    begin = dc.indexOf(cname);       
        if (begin != -1) {           
        begin += cname.length;       
        end = dc.indexOf(";", begin);
            if (end == -1) end = dc.length;
            return unescape(dc.substring(begin, end));
        } 
    }
return null;
}

// Use this function to save a cookie.
function setCookie(name, value, expires) {
document.cookie = name + "=" + escape(value) + "; path=/" +
((expires == null) ? "" : "; expires=" + expires.toGMTString());
}

// Use this function to delete a cookie.
function delCookie(name) {
document.cookie = name + "=; expires=Thu, 01-Jan-70 00:00:01 GMT" +  "; path=/";
}



// Function to retrieve form element's value.
function getValue(element) {
var value = getCookie(element.name);
    if (value != null) element.value = value;
}

// Function to save form element's value.
function setValue(element) {
setCookie(element.name, element.value, exp);
}

var exp = new Date();                                   
exp.setTime(exp.getTime() + (1000 * 60 * 60 * 24 * 31));
//--> 
</script>
</head>
<center><h1>A Form that Remembers</h1></center>
<p>Type whatever you like in the boxes below, click outside the box, reload the page and try to type the details again. This script uses cookies, so if you have them disabled it will of course not work.</p>
<form><table>
<tr>
  <td>Your Name: </td>
  <td><input type="text" name="yourname" onfocus="getValue(this)" onblur="setValue(this)"></td>
</tr>
<tr>
  <td>Your ID: </td>
  <td><input type="text" name="yourid" onfocus="getValue(this)" onblur="setValue(this)"></td>
</tr>
<tr>
  <td>Your Password: </td>
  <td><input type="text" name="yourpassword" onfocus="getValue(this)" onblur="setValue(this)"></td>
</tr>
</table>
<input type="submit" value="Submit">
</form>
<?php
print_r($_POST);