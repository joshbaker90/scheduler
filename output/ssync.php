<div class="container-wrap">

<!-- Progress bar holder -->
<div id="progress" style="width:500px;border:1px solid #ccc;"></div>
<!-- Progress information -->
<div id="information" style="width"></div>

<?php
  require_once '../../../../wp-config.php';
  require_once '../../../../wp-includes/wp-db.php';
  require_once '../podio/config.php';
  require_once '../podio/PodioAPI.php';
  require_once '../podio/authorise.php';

Podio::authenticate('app', array('app_id' => '8327586', 'app_token' => '8ff069bbe1cc478090b0d9ba29c8d4c9'));
//  Podio::$debug = true;
$prefix = 'swp_';
global $wpdb;
$wpprefix = $wpdb->prefix;
 
error_log("update activity table triggered");  

$item_array = PodioItem::filter_by_view(8327586, 26209843, $attributes = array('limit' => 500) );
$pcount = PodioItem::get_count( 8327586 );
global $wpdb;
$topiccount = $wpdb->get_results("SELECT * FROM ".$wpprefix."term_taxonomy WHERE taxonomy = 'topics'", OBJECT );
$topicscounted = count($topiccount);
// Total processes
$total = $pcount + $topicscounted;
// Loop through process
for($i=1; $i<=$total; $i++){
    // Calculate the percentation
    $percent = intval($i/$total * 100)."%";
    
    // Javascript for updating the progress bar and information
    echo '<script language="javascript">
    document.getElementById("progress").innerHTML="<div style=\"width:'.$percent.';background-color:#ddd;\">&nbsp;</div>";
    document.getElementById("information").innerHTML="'.$i.'/'.$total.' Activities synced with wordpress.";
    </script>';
    

// This is for the buffer achieve the minimum size in order to flush data
    echo str_repeat(' ',1024*64);
    

// Send output to browser immediately
    flush();
    

// Sleep one second so we can see the delay
    sleep(0.6);
}


foreach ($item_array as $item) {
$item_id = $item->item_id;
$app_item_id = $item->app_item_id;
$postname = $item_id;
unset($activity);
unset($topicarray);
unset($description);
unset($goal);
unset($equipment);
unset($duration);
unset($linksarray);
unset($imagearray);
unset($urlimages);
unset($urllinks);


//get activity activity
if(isset($item->fields['activity']->values))
{ 
$oactivity = $item->fields['activity']->values;
$activity = str_replace("'","(apos)","$oactivity");

}
else { $activity ="";
}


global $wpdb;
$query = $wpdb->get_results( "SELECT * FROM ".$wpprefix."posts WHERE post_name = '$postname'", OBJECT );
if(count($query) == 0)
{

require_once (ABSPATH."wp-content/plugins/scheduler/webhooks/create.php");

print $item->app_item_id ;
echo ' ';
print $activity;
print ' created successfully<br>';

}
else 
{

require_once (ABSPATH."wp-content/plugins/scheduler/webhooks/update.php");

}


}



//UPDATE COUNT topic count//
global $wpdb;
$ttaxonomy= $wpdb->get_results( "SELECT * FROM ".$wpprefix."term_taxonomy WHERE taxonomy='topics'", OBJECT );

foreach ($ttaxonomy as $ttax) {
$termid = $ttax->term_id;

$tcount= $wpdb->get_results( "SELECT * FROM ".$wpprefix."term_relationships WHERE term_taxonomy_id='$termid'", OBJECT );
$termcount = count($tcount);

$wpdb->update( 
  $wpprefix.'term_taxonomy', 
  array( 
    'count' => $termcount,  // integer (number)
    
  ), 
  array( 'term_id' => $termid ), 
  array( 
    '%d'  // value1
  ), 
  array( '%d' ) 
);
}

//DELETE SCRIPT//
global $wpdb;
$query = $wpdb->get_results( "SELECT * FROM ".$wpprefix."posts WHERE post_type ='activities'", OBJECT );
foreach($query as $row){
$podioid = $row->post_name;
$postid = $row->ID;

$deleteitemnotfound = 'no';
$type = 'activities';
$dargs=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'ignore_sticky_posts'=> 1);

try {
  if ($item = PodioItem::get( $podioid )) {
    // The item exists
        //do nothing
  }
} catch (PodioNotFoundError $nfe) {
  // The item didn't exist
if ($deleteitemnotfound == 'yes') {
echo $postid .' Activity' .$podioid. ' doesnt exists in the podio database, ';
new WP_Query($dargs);
wp_delete_post($postid);
wp_reset_query();
echo 'this Activity will be deleted<br>';
}
else {
echo $postid .' Activity ' .$podioid. ' was not found in the podio database but the settings are set to not delete unrecognised items. <br>';
}


}
catch (PodioGoneError $ge) {
  // The item didn't exist
echo 'item' .$podioid. ' no longer exists and will be deleted, ';
new WP_Query($dargs);
wp_delete_post($postid);
wp_reset_query();
}
}

// Tell user that the process is completed
echo '<script language="javascript">document.getElementById("information").innerHTML="Process completed"</script>';



?>
</div>