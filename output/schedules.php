<?php
/*template name: Admin Activities*/

$sprefix = 'swp_';

//wordpress query args
if (isset($_POST['quantity'])) {
if ($_POST['quantity'] == 'ALL' OR $_POST['quantity'] == '') {
$quantity = 'ALL';  $quantityargs = -1;
} else {
$quantity = $_POST['quantity']; $quantityargs = $_POST['quantity']; }
} else { $quantity = 'ALL';  $quantityargs = -1; }

if (isset($_POST['order'])) {
$order = $_POST['order'];
} else { $order = 'ASC'; }

//Get topics taxonomy
$taxonomy = 'topics';
$term_args=array(
  'hide_empty' => false,
  'orderby' => 'name',
  'order' => 'ASC'
);
$tax_terms = get_terms($taxonomy,$term_args);
foreach ($tax_terms as $tax_term) {
$topicsarray[] = $tax_term->name;
}
//print_r ($topicsarray);

//Get groups taxonomy
$taxonomy = 'groups';
$term_args=array(
  'hide_empty' => false,
  'orderby' => 'name',
  'order' => 'ASC'
);
$tax_terms = get_terms($taxonomy,$term_args);
foreach ($tax_terms as $tax_term) {
$groupsarray[] = $tax_term->name;
}


$type = 'programmes';
if (isset($_POST['clear'])) {
$args=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'ignore_sticky_posts'=> 1,
  'orderby'   => 'title',
  'order' => 'ASC',
    );

$quantity = 'ALL';
}

else {


$args=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => $quantityargs,
  'ignore_sticky_posts'=> 1,
  'orderby'   => 'title',
  'order' => $order,
    );

//add topic array to $args
if (isset($_POST['topics'])) {
if ($_POST['topics'] == '') {
}
else
{
$topics = $_POST['topics'];
$args['tax_query'][0]['taxonomy'] = 'topics';
$args['tax_query'][0]['field'] = 'slug';
$args['tax_query'][0]['terms'] = $topics;
}
}

//add groups array to $args
if (isset($_POST['groups'])) {
if ($_POST['groups'] == '') {
}
else
{
$groups = $_POST['groups'];
$args['tax_query'][1]['taxonomy'] = 'groups';
$args['tax_query'][1]['field'] = 'slug';
$args['tax_query'][1]['terms'] = $groups;
}
}



}

?>
<div class="filterformactivities">
<div class="ywaligncenter">

<form class="filterform filterform-s" action="" method="post">

<div class="formdivs">
<h2>Quantity</h2>
<input class="filterfield filterfield-qi" name="quantity" type"number" value="<?php echo $quantity; ?>">
</div>

<div class="formdivs">
<h2>Topic</h2>
<select class="filterfield filterfield-topic" name ="topics" value = "$topics">
<option value ="">...</option>
<?php
foreach ($topicsarray as $topic) {
echo '<option value ="'.$topic.'"'; if (isset($topics)) { if ($topics == $topic) { echo "selected=selected";}} echo'>'.$topic.'</option>';
}
?>
</select>
</div>

<div class="formdivs">
<h2>Group</h2>
<select class="filterfield filterfield-group" name ="groups" value = "$groups">
<option value ="">...</option>
<?php
foreach ($groupsarray as $group) {
echo '<option value ="'.$group.'"'; if (isset($groups)) { if ($groups == $group) { echo "selected=selected";}} echo'>'.$group.'</option>';
}
?>
</select>
</div>

<div class="formdivs">
<h2>Order</h2>
<select class="filterfield filterfield-order" name ="order" value = "$order">
<option value ="ASC" <?php if ($order == 'ASC') { echo "selected=selected"; } ?>>ASC</option>
<option value ="DESC" <?php if ($order == 'DESC') { echo "selected=selected"; } ?>>DESC</option>
</select>
</div>

<div class="formdivs">
<input class="ywsubmit" type="submit" value="submit">
</div>

<div class="formdivs">
<input class="ywsubmit" name="clear" type="submit" value="clear">
</div>
</form>

</div>
</div>

<?php
$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
  while ($my_query->have_posts()) : $my_query->the_post(); ?>
    <p><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"></a></p>
 
<?php
//Get content

$title = get_the_title();
$topic = get_the_term_list($my_query->ID, 'topics', '', ', ' );
$description = get_the_content('more');
$swpschedule = get_post_meta(get_the_ID(), $sprefix.'schedule', true);
$swproles = get_post_meta(get_the_ID(), $sprefix.'roles', false);
$swpoldschedules = get_post_meta(get_the_ID(), $sprefix.'schedule', false);
$id = get_the_ID();
$wpurl= site_url();

?>

<div class='content'>
<div class='inline'>

<div id="div1<?php echo "$id" ?>" class="targetDiv">
<p class="inline-block">Are you sure you want to delete this activity?</p>

<button class="delete-button-no showSingle float-right" target="1<?php echo "$id" ?>"><?php echo "<img src='".plugins_url('icons/delete99.png', __FILE__ )."'   style='width:16px;height:16px' alt='Delete'>"; ?></button>

<form class="inline-block float-right" action="<?php echo get_delete_post_link( $post->ID ) ?>" method="post">
<input name="unique_id" type="hidden" value="$id">
<input name="podio_id" type="hidden" value="$Podioid">
<input name="pagename" type="hidden" value="$pagename">
<button class="editactivity showSingle float-right" target="1<?php echo "$id" ?>"><?php echo "<img src='".plugins_url('icons/done.png', __FILE__ )."'   style='width:16px;height:16px' alt='Delete'>"; ?></button></form>

</div>

<div class="inline float-right">
<button class="delete-button-no showSingle inline float-right" target="1<?php echo "$id" ?>"><?php echo "<img src='".plugins_url('icons/delete81.png', __FILE__ )."'   style='width:16px;height:16px' alt='Delete'>"; ?></button>
</div>

<div class="inline activitylayer float-right">
<form class="inline-block float-right" action="<?php echo esc_url( home_url( '/wp-admin/post.php?post='.$id.'&action=edit' ) ); ?>" method="post" target="_blank">
<button class='editactivity float-right' value='toggle'><?php echo "<img src='".plugins_url('icons/pencil112.png', __FILE__ )."'   style='width:16px;height:16px' alt='Edit'>"; ?></button></form>
</div>

<div class="inline activitylayer float-right">
<form class="inline-block float-right" action="<?php echo $wpurl; ?>/activity_item" method="post" target="_blank">
<input name="unique_id" type="hidden" value="<?php echo $id; ?>">
<button class='editactivity float-right' value='toggle'><?php echo "<img src='".plugins_url('icons/printer67 (1).png', __FILE__ )."'   style='width:16px;height:16px' alt='Edit'>"; ?></button></form>
</div>

<div class="inline float-right">
<button class="toggle showSingle inline float-right" target="2<?php echo "$id" ?>"><?php echo "<img src='".plugins_url('icons/expand.png', __FILE__ )."'   style='width:16px;height:16px' alt='Edit'>"; ?></button>
</div>

<?php

//Activity duration
if(empty($duration))
{ }
else {
echo "<div class='inline'><div class='durationwrap'>
<span class='durationtext'>" . $duration . " minutes </span>
<span class='durationimg'><img src='" . plugins_url( 'icons/chronometer10.png', __FILE__ ) . "'style='width:16px;height:16px' alt='Duration:'></span>
</div></div>";
}

//Activity topic
if(empty($topic))
{ }
else {
echo "<div class='inline float-right'><div class='topicwrap'>
<span class='topic'><b>Topic: </b>".$topic."</span>
</div></div>";
}

?>

</div>

<?php
//echo the content

//Activity title
if(empty($title))
{ 

}
else { 
echo "<div class='inline'><h3><span class='activitytitle'>".$title."</span></h3></div>";
}
?>

<?php
if(empty($description))
{ 

}
else { 
echo "<br><div class='items'>
<p><span class='fieldtitle'><b>Description:&nbsp;</b></span></p><span class='fieldcontent'>".$description."</span></div>";
}
?></div>

<!-- Activity image
//  echo '<span>Image:'  . $row['image'] . "" . "</span>";  -->

<div class='items'>

</div>

<div id="div2<?php echo "$id" ?>" class="targetDiv">
<br><br>
<!-- Schedule -->
<?php

echo '<div id="Div" class="swp_frame_page">
<div class="swp_schedule_time"><h2>Time: <h2></div>
<div class="swp_schedule_activity"><h2>Activity: <h2></div>
<div class="swp_schedule_user"><h2>Person: <h2></div>
<div class="swp_schedule_person"><h2>Other Person(s): <h2></div>
</div>';

if(empty($swpoldschedules[0])){

}
else {

$swpoldschedule = unserialize(urldecode($swpoldschedules[0]));

foreach ($swpoldschedule as $item){
if(empty($item['swp_starttime'])){
$ostarttime = ""; 
}
else {
if($item['swp_starttime']=="12:00"){
$ostarttime = $item['swp_starttime']." NOON";
} elseif($item['swp_starttime']=="00:00"){
$ostarttime = $item['swp_starttime']." MIDNIGHT";
} elseif ($item['swp_starttime']<"12:00") {
$ostarttime = $item['swp_starttime']."AM";
} elseif ($item['swp_starttime']>"12:00") {
$ostarttime = $item['swp_starttime']."PM";
} else {
$oendtime = ""; 
}
}

if(empty($item['swp_endtime'])){
$oendtime = ""; 
}
else {
if($item['swp_endtime']=="12:00"){
$oendtime = $item['swp_endttime']." NOON";
} elseif ($item['swp_endtime']=="00:00"){
$oendtime = "00:00 MIDNIGHT"; 
} elseif ($item['swp_endtime']<"12:00") {
$oendtime = $item['swp_endtime']."AM";
} elseif ($item['swp_endtime']>"12:00") {
$oendtime = $item['swp_endtime']."PM";
} else {
$oendtime = ""; 
}
}

if(isset($item['swp_activity'])){
$oactivity = $item['swp_activity'];
$oactivity = explode(" ", "$oactivity", 2);
$oactivity = $oactivity[1];
$activityid = $oactivity[2];
}
else { $oactivity = ""; }

if(isset($item['swp_persons'])){
$opersons = $item['swp_persons'];  
}
else { $opersons = ""; }

if(isset($item['swp_person'])){
$ouser = $item['swp_person'];  
}
else { $ouser = ""; }



echo '<div id="Div" class="swp_frame_page">
<div class="swp_schedule_time">'; if(empty($ostarttime)){ echo "00:00";} else { echo $ostarttime; } echo " - "; if(empty($oendtime)){ echo "00:00";} else { echo $oendtime; }  echo'</div>
<div class="swp_schedule_activity">'; if(empty($oactivity)){ echo " - ";} else { echo $oactivity; } echo'</div>
<div class="swp_schedule_user">'; if(empty($ouser)){ echo " - ";} else { echo $ouser; } echo'</div>
<div class="swp_schedule_person">'; if(empty($opersons)){ echo " - ";} else { echo $opersons; } echo'</div>

</div>';
}}

?>


<!-- Schedule Roles -->
<?php
echo '<br><br>';

if(empty($swproles[0])){

}
else {

$swproles = unserialize(urldecode($swproles[0]));

foreach ($swproles as $item){
if(isset($item['swp_role'])){
$role = $item['swp_role']; 
}
else { $role = ""; }

if(isset($item['swp_role_persons'])){
$persons = $item['swp_role_persons'];  
}
else { $persons = ""; }

if(isset($item['swp_role_person'])){
$ouser = $item['swp_role_person'];  
}
else { $ouser = ""; }


echo '<div id="Div" class="swp_frame">
<span>Roles: '.$role.'</span>
Person: '.$ouser.'
Other Person(s): '.$opersons.'
</div>';
}}

?>


<!-- echo "<div class=\"pinterest\"><a href=\"//www.pinterest.com/pin/create/button/\" data-pin-do=\"buttonBookmark\"  data-pin-color=\"red\" data-pin-height=\"28\"><img src=\"//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_red_28.png\" /></a>
--><!-- Please call pinit.js only once per page -->
<!-- <script type=\"text/javascript\" async defer src=\"//assets.pinterest.com/js/pinit.js\"></script></div>";-->

</div>
<br><br><hr></div>



<?php
  endwhile;
}
wp_reset_query();  // Restore global post data stomped by the_post().
?>