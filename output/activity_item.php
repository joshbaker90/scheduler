<?php
if (isset($_POST["unique_id"])) {
$id = $_POST["unique_id"];
}
else { $id = ''; }

$my_query = null;
$sprefix = 'swp_';
$type = 'activities';
$order = 'ASC';
$id = $id;

$args=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'ignore_sticky_posts'=> 1,
  'orderby'   => 'title',
  'p' => $id,
    );


$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
  while ($my_query->have_posts()) : $my_query->the_post(); ?>
    <p><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"></a></p>

<?php
//Get content
$title = get_the_title();
$topic = get_the_term_list($my_query->ID, 'topics', '', ', ' );
$duration = get_post_meta(get_the_ID(), $sprefix.'duration', true);
$description = get_the_content('more');
$goal = get_post_meta(get_the_ID(), $sprefix.'goal', true);
$equipment = get_post_meta(get_the_ID(), $sprefix.'equipment', true);
$link = get_post_meta(get_the_ID(), $sprefix.'link_url', true);
$id = get_the_ID();
$wpurl= site_url();
?>

<?php
echo '
<head>
   <link href="http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext" rel="stylesheet" type="text/css">
   <link href="'.$wpurl.'/wp-content/plugins/scheduler/css/style.css" rel="stylesheet" type="text/css">

     </head>

<div id="swp_item">

<div id="swp_header">

    <div class="swp_item_header"><h1>'.strtoupper($title).'</h1></div>
    <div class="swp_item_print">
  	<img onclick="window.print()" class="swp_icon_print" src="' . plugins_url( "icons/printer67 (2).png", __FILE__ ) . '" style="width:32px;height:32px" alt="print:">
  	</div>

</div>



<div class="swp_block">';

if(empty($topic)) {} else {
echo '
<div class=""><div class="topicwrap1">
<span><b>Topic: </b>'.$topic.'</span>
</div></div>';
}

if(empty($duration)) {} else {
echo '
<div class=""><div class="durationwrap1">
<span class="durationimg1"><img src="' . plugins_url( "icons/chronometer10.png", __FILE__ ) . '" style="width:16px; height:16px" alt="Duration: "></span>
<span class="durationtext1">' . $duration . ' minutes </span>
</div></div>
</div>';
}

if(empty($goal)) {} else {
echo '
<div class="swp_block">
<div class="inline">
<div class="swp_standard_space">
<b>Goal:</b> '.strip_tags("$goal", "").'
</div>
</div>
</div>';
}

if(empty($equipment)) {} else {
echo '
<div class="swp_block">
<div class="swp_standard_space">
<b>Equipment:</b><ul> '.strip_tags(str_replace("<p>", "<li>", "$equipment"), "<<li>,</li>,<ul>,</ul>").'</ul>
</div></div>';
}

if(empty($description)) {} else {
echo '
<div class="swp_block">
<div class="swp_standard_space">
<b>Description:</b> '.str_replace("(apos)","''","$description").'
</div></div>';
}

if(empty($link)) {} else {
echo '
<div class="swp_block">
<div class="swp_standard_space">
<b>Links: </b><a href="'.$link.'" target="blank">'.$link.'</a>
</div></div>';
}

echo '
</div>
<hr class="swp_item_hr">
</div>
';


endwhile;
}
wp_reset_query();  // Restore global post data stomped by the_post().
?>

<!-- 



-->