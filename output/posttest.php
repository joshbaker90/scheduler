<?php
$sprefix = 'swp_';

//wordpress query args
if (isset($_POST['quantity'])) {
if ($_POST['quantity'] == 'ALL' OR $_POST['quantity'] == '') {
$quantity = ALL;  $quantityargs = -1;
} else {
$quantity = $_POST['quantity']; $quantityargs = $_POST['quantity']; }
} else { $quantity = ALL;  $quantityargs = -1; }

if (isset($_POST['order'])) {
$order = $_POST['order'];
} else { $order = 'ASC'; }

//Get topics taxonomy
$taxonomy = 'topics';
$term_args=array(
  'hide_empty' => false,
  'orderby' => 'name',
  'order' => 'ASC'
);
$tax_terms = get_terms($taxonomy,$term_args);
foreach ($tax_terms as $tax_term) {
$topicsarray[] = $tax_term->name;
}
//print_r ($topicsarray);

//Get groups taxonomy
$taxonomy = 'groups';
$term_args=array(
  'hide_empty' => false,
  'orderby' => 'name',
  'order' => 'ASC'
);
$tax_terms = get_terms($taxonomy,$term_args);
foreach ($tax_terms as $tax_term) {
$groupsarray[] = $tax_term->name;
}


$type = 'activities';
if (isset($_POST['clear'])) {
$args=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'caller_get_posts'=> 1,
  'orderby'   => 'title',
  'order' => 'ASC',
    );

$quantity = 'ALL';
}

else {


$args=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => $quantityargs,
  'caller_get_posts'=> 1,
  'orderby'   => 'title',
  'order' => $order,
    );

//add topic array to $args
if (isset($_POST['topics'])) {
if ($_POST['topics'] == '') {
}
else
{
$topics = $_POST['topics'];
$args['tax_query'][0]['taxonomy'] = 'topics';
$args['tax_query'][0]['field'] = 'slug';
$args['tax_query'][0]['terms'] = $topics;
}
}


//add groups array to $args
if (isset($_POST['groups'])) {
if ($_POST['groups'] == '') {
}
else
{
$groups = $_POST['groups'];
$args['tax_query'][1]['taxonomy'] = 'groups';
$args['tax_query'][1]['field'] = 'slug';
$args['tax_query'][1]['terms'] = $groups;
}
}



}

?>
<div class="filterformactivities">
<div class="ywaligncenter">

<form class="filterform filterform-s" action="" method="post">

<div class="formdivs">
<h2>Quantity</h2>
<input class="filterfield filterfield-qi" name="quantity" type"number" value="<?php echo $quantity; ?>">
</div>

<div class="formdivs">
<h2>Topic</h2>
<select class="filterfield filterfield-topic" name ="topics" value = "$topics">
<option value ="">...</option>
<?php
foreach ($topicsarray as $topic) {
echo '<option value ="'.$topic.'"'; if ($topics == $topic) { echo "selected=selected";} echo'>'.$topic.'</option>';
}
?>
</select>
</div>

<div class="formdivs">
<h2>Group</h2>
<select class="filterfield filterfield-group" name ="groups" value = "$groups">
<option value ="">...</option>
<?php
foreach ($groupsarray as $group) {
echo '<option value ="'.$group.'"'; if ($groups == $group) { echo "selected=selected";} echo'>'.$group.'</option>';
}
?>
</select>
</div>

<div class="formdivs">
<h2>Order</h2>
<select class="filterfield filterfield-order" name ="order" value = "$order">
<option value ="ASC" <?php if ($order == 'ASC') { echo "selected=selected"; } ?>>ASC</option>
<option value ="DESC" <?php if ($order == 'DESC') { echo "selected=selected"; } ?>>DESC</option>
</select>
</div>

<div class="formdivs">
<input class="ywsubmit" type="submit" value="submit">
</div>

<div class="formdivs">
<input class="ywsubmit" name="clear" type="submit" value="clear">
</div>
</form>

</div>
</div>



<?php
$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
  while ($my_query->have_posts()) : $my_query->the_post(); ?>
    <p><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"></a></p>
 


<?php

//Get content
$title = get_the_title();
$topic = get_the_term_list( $post->ID, 'topics', '', ', ' );
$duration = get_post_meta(get_the_ID(), $sprefix.'duration', true);
$description = get_the_content('more');
$goal = get_post_meta(get_the_ID(), $sprefix.'goal', true);
$equipment = get_post_meta(get_the_ID(), $sprefix.'equipment', true);
$link = get_post_meta(get_the_ID(), $sprefix.'link_url', true);
$id = get_the_ID();

?>



<div class='content'>
<div class='inline'>

<div class="inline float-right">

<button class="toggle showSingle inline float-right" target="4<?php echo "$id" ?>">...</button>
</div>

<?php

//Activity duration
if(empty($duration))
{ }
else {
echo "<div class='inline'><div class='durationwrap'>
<span class='durationtext'>" . $duration . " minutes </span>
<span class='durationimg'><img src='" . plugins_url( 'icons/chronometer10.png', __FILE__ ) . "' style='width:16px;height:16px' alt='Duration:'></span>
</div></div>";
}

//Activity topic
if(empty($topic))
{ }
else {
echo "<div class='inline float-right'><div class='topicwrap'>
<span class='topic'><b>Topic: </b>".$topic."</span>
</div></div>";
}

?>

</div>

<?php
//echo the content

//Activity title
if(empty($title))
{ 

}
else { 
echo "<div class='inline'><h3><span class='activitytitle'>".$title."</span></h3></div>";
}
?>




<!-- Activity goal -->
<?php
if(empty($goal))
{ 

}
else { 
echo "<br><div class='items'>
<p><span class='fieldtitle'><b>Goal:&nbsp;</b></span></p><span class='fieldcontent'>".$goal."</span></div>";
}
?>



<!-- Activity image
//  echo '<span>Image:'  . $row['image'] . "" . "</span>";  -->

<div class='items'>

</div>

<div id="div4<?php echo "$id" ?>" class="targetDiv">


<?php

// Activity  description
if(empty($description))
{}
else { 
echo "<br><div class='items'>
<p><span class='fieldtitle'><b>Description:&nbsp;</b></span></p><span class='fieldcontent'>".$description."</span></div>";
}


// Activity Equipment
if(empty($equipment))
{}
else { 
echo "<br><div class='items'>
<p><span class='fieldtitle'><b>Equipment:&nbsp;</b></span></p><span class='fieldcontent'>".$equipment."</span></div>";
}


// Activity Link
if(empty($link))
{}
else {
$urls = explode(',', $link);
foreach($urls as $link){

echo "<br><div class='items'>
<p><span class='fieldtitle'><b>Link:&nbsp;</b></span></p><span class='fieldcontent'><a href ='".$link."' target='blank'>".$link."</a></span></div>";
}
}
?>


<!-- echo "<div class=\"pinterest\"><a href=\"//www.pinterest.com/pin/create/button/\" data-pin-do=\"buttonBookmark\"  data-pin-color=\"red\" data-pin-height=\"28\"><img src=\"//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_red_28.png\" /></a>
--><!-- Please call pinit.js only once per page -->
<!-- <script type=\"text/javascript\" async defer src=\"//assets.pinterest.com/js/pinit.js\"></script></div>";-->

</div>
<br><br><hr></div>



<?php
  endwhile;
}
wp_reset_query();  // Restore global post data stomped by the_post().
?>