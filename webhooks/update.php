<?php

$sprefix = 'swp_';
global $wpdb;
$wpprefix = $wpdb->prefix;

  // Turn on debugging
Podio::$debug = true;

error_log("update item triggered");

$item = PodioItem::get( $podioid );
//get app_item_id (index no)
$app_item_id = $item->app_item_id;


if(isset($item->fields['sync']->values[0]['text']))
{
$sync = $item->fields['sync']->values[0]['text'];
}
else {
$sync = 'Yes'; 
}

if ($sync=='Yes'){

require 'fielddata.php';

$wppodioid = get_post_meta($wppostid, "$sprefix".'podioid', true);
$wporigin = get_post_meta($wppostid, "$sprefix".'origin', true);

$result = $wpdb->get_results( "SELECT * FROM ".$wpprefix."postmeta WHERE meta_value = '$podioid'", OBJECT );
if(count($result) == 0)
{
//create
if(empty($wppodioid) && ($wporigin === $origin))
{   //create item
$case = "createpid"; }

if(($wppodioid !== $podioid) && ($wporigin !== $origin))
{   //create item
$case = "createitem"; }

switch ($case) {
  case 'createpid':
//Create podio id
  break;
  case 'createitem':

$podio = "podio";
//If record doesnt exist: create record:
$new_activitypost = array(
  'post_title'    => $activity,
  'post_content'  => $description,
  'post_status'    => $publish,
  'comment_status' => 'open',
  'ping_status'    => 'open',
  'post_name'      => 'newpodioactivity',
  'post_type'      => 'activities',
  'post_date'  => $postdate,
  'post_password' => $post_password
);
$postid = wp_insert_post($new_activitypost);

add_post_meta($postid, "$sprefix".'lastupdatedby', "podio", true);
add_post_meta($postid, "$sprefix".'podioid', $podioid, true);
add_post_meta($postid, "$sprefix".'goal', $goal, true);
add_post_meta($postid, "$sprefix".'equipment', $equipment, true);
add_post_meta($postid, "$sprefix".'duration', $duration, true);
add_post_meta($postid, "$sprefix".'link_url', $urllinks, true);

if ($new_post_id == 0) {
    //echo 'Could not create the post.';
}
else {
    //echo 'New post'.$activity.' created. ';
}

unset($new_activitypost);

//update the activity topic
global $wpdb;
$wpdb->query($wpdb->prepare("DELETE FROM ".$wpprefix."term_relationships WHERE object_id = %d",$postid));
if(isset($topicarray)) {
foreach ($topicarray as $topictitle) {

global $wpdb;
$ttid = $wpdb->get_results( "SELECT * FROM ".$wpprefix."terms WHERE name = '$topictitle'", OBJECT );
if(count($ttid) == 0)
{ // If topic isnt found...


$slug = str_replace(" ", "-", $topictitle);
global $wpdb;
$wpdb->insert(
  $wpprefix.'terms', 
  array( 
    'name' => $topictitle, 
    'slug' => $slug,
    'term_group' => 0
  ), 
  array(
    '%s', 
    '%s',
    '%d' 
  ) 
);

$ttid = $wpdb->get_results( "SELECT * FROM ".$wpprefix."terms WHERE name = '$topictitle'", OBJECT );
$termid = $ttid[0]->term_id;
}
else
  { //if topic is found...
$termid = $ttid[0]->term_id;
}

$ttaxonomy = $wpdb->get_results( "SELECT * FROM ".$wpprefix."term_taxonomy WHERE term_id = '$termid'", OBJECT );
if(count($ttaxonomy) == 0)
{ // If ttaxonomy isnt found...
global $wpdb;
$wpdb->insert( 
  $wpprefix.'term_taxonomy', 
  array( 
    'term_taxonomy_id' => $termid, 
    'term_id' => $termid,
    'taxonomy' => 'topics',
    'description' => 0,
    'parent' => 0,
    'count' => 1
    ), 
  array( 
    '%d',
    '%d', 
    '%s',
    '%d',
    '%d',
    '%d' 
  ) 
);
}


global $wpdb;
$wpdb->insert( 
  $wpprefix.'term_relationships', 
  array( 
    'object_ID' => $postid, 
    'term_taxonomy_id' => $termid
  )
);

}}
unset($topicarray);

global $wpdb;
$wpdb->update( "$wpprefix"."posts", array( 'post_modified' => $pmd), array( 'ID' => $postid ), array( '%s' ) );

echo "published post created";

break;
}

}
else
{

global $wpdb;
$query = $wpdb->get_results( "SELECT * FROM ".$wpprefix."postmeta WHERE meta_value = '$podioid'", OBJECT );
$postid = $query[0]->post_id;

$postdata = $wpdb->get_results( "SELECT * FROM ".$wpprefix."posts WHERE ID = '$postid'", OBJECT );
$postmodified = $postdata[0]->post_modified;

$podiodate=strtotime($podiodate);
$postmodified=strtotime($postmodified);

if ($podiodate < $postmodified) {
require_once(ABSPATH.'wp-content/plugins/scheduler/podiohooks/fielddata.php');
//require_once(ABSPATH.'wp-content/plugins/scheduler/podiohooks/update.php');

echo $item->app_item_id." Wordpress updated podio library with ".$activity."<br><br>";
}
else if ($podiodate == $postmodified) {
  //do nothing
 echo $item->app_item_id." no need to update ".$activity."<br><br>";
}

else if ($podiodate > $postmodified) {
//update post


// Update post 37
  $update_activitypost = array(
      'ID'           => $postid,
      'post_title'   => $activity,
      'post_content' => $description,
      'post_date'  => $postdate,
      'post_status'    => $publish,
      'post_name'      => 'updatepodioactivity',
      'post_date'  => $postdate,
      'post_password' => $post_password
  );

// Update the post into the database
  wp_update_post( $update_activitypost );

  unset($update_activitypost);

//update the activity topic
global $wpdb;
$wpdb->query($wpdb->prepare("DELETE FROM ".$wpprefix."term_relationships WHERE object_id = %d",$postid));
if(isset($topicarray)) {
foreach ($topicarray as $topictitle) {

global $wpdb;
$ttid = $wpdb->get_results( "SELECT * FROM ".$wpprefix."terms WHERE name = '$topictitle'", OBJECT );
if(count($ttid) == 0)
{ // If topic isnt found...

$slug = str_replace(" ", "-", $topictitle);
global $wpdb;
$wpdb->insert(
  $wpprefix.'terms', 
  array( 
    'name' => $topictitle, 
    'slug' => $slug,
    'term_group' => 0
  ), 
  array(
    '%s', 
    '%s',
    '%d' 
  ) 
);

$ttid = $wpdb->get_results( "SELECT * FROM ".$wpprefix."terms WHERE name = '$topictitle'", OBJECT );
$termid = $ttid[0]->term_id;
}
else
  { //if topic is found...
$termid = $ttid[0]->term_id;
}

$ttaxonomy = $wpdb->get_results( "SELECT * FROM ".$wpprefix."term_taxonomy WHERE term_id = '$termid'", OBJECT );
if(count($ttaxonomy) == 0)
{ // If ttaxonomy isnt found...
global $wpdb;
$wpdb->insert( 
  $wpprefix.'term_taxonomy', 
  array( 
    'term_taxonomy_id' => $termid, 
    'term_id' => $termid,
    'taxonomy' => 'topics',
    'description' => 0,
    'parent' => 0,
    'count' => 1
    ), 
  array( 
    '%d',
    '%d', 
    '%s',
    '%d',
    '%d',
    '%d' 
  ) 
);
}


global $wpdb;
$wpdb->insert( 
  $wpprefix.'term_relationships', 
  array( 
    'object_ID' => $postid, 
    'term_taxonomy_id' => $termid
  )
);

}}
unset($topicarray);

$lastupdatedby = get_post_meta($wppostid, "$sprefix".'lastupdatedby', true);
if(empty($lastupdatedby)) {
add_post_meta($postid, "$sprefix".'lastupdatedby', 'podio', true);
//echo "lastupdatedby added";
}
  else
{
update_post_meta($postid, "$sprefix".'lastupdatedby', 'podio', true);
//echo "lastupdatedby update to podio";
}

update_post_meta($postid, "$sprefix".'goal', $goal, false);
update_post_meta($postid, "$sprefix".'equipment', $equipment, false);
update_post_meta($postid, "$sprefix".'duration', $duration, false);
update_post_meta($postid, "$sprefix".'link_url', $urllinks, false);

global $wpdb;
$wpdb->update( $wpprefix.'posts', array( 'post_modified' => $pmd), array( 'ID' => $postid ), array( '%s' ) );

echo $item->app_item_id." updated ".$activity." successfully<br>";

}


else {
echo $item->app_item_id." timestamp update query failed for ".$activity."<br>";  
}

}
echo "<hr>";
}

else {
//if record exists delete it
$result = $wpdb->get_results( "SELECT * FROM ".$wpprefix."postmeta WHERE meta_value = '$podioid'", OBJECT );
if(count($result) == 0)
{
//echo "record doesnt exist";
}
else
{
foreach ($result as $row) {
$postid = $row->post_id;
$type = 'activities';
$args=array(
  'post_type' => $type,
  'posts_per_page' => -1,
  'ignore_sticky_posts'=> 1);

new WP_Query($args);
wp_delete_post($postid);
wp_reset_query();

//mysqli_query($con,"DELETE FROM Activities WHERE Podioid='$podioid'");

echo "non post deleted";
}
}

$ids = $postid;
}