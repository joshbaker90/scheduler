<?php
  require_once '../../../../wp-config.php';
  require_once '../../../../wp-includes/wp-db.php';
  require_once '../podio/config.php';
  require_once '../podio/PodioAPI.php';
  require_once '../podio/authorise.php';

$url = get_site_url();
$hookurl = '/wp-content/plugins/scheduler/webhooks/hook.php';


if (preg_match('/localhost/',$url) || preg_match('/127.0.0.1/',$url)) {
$externalContent = file_get_contents('http://checkip.dyndns.com/');
preg_match('/Current IP Address: \[?([:.0-9a-fA-F]+)\]?/', $externalContent, $m);
$externalIp = $m[1];


if (preg_match('/localhost/',$url)) {
$url1 = str_replace('localhost', $externalIp, $url);
//str_replace('low', 'high', $oldurl);
$hook_url = $url1 . $hookurl;
}

if (preg_match('/127.0.0.1/',$url)) {
$url1 = str_replace('localhost', $externalIp, $url);
//str_replace('low', 'high', $oldurl);
$hook_url = $url1 . $hookurl;
}


} else {
$hook_url = $url . $hookurl;
}


$hooks = PodioHook::get_for('app', 8327586 );

foreach ($hooks as $hook) {
  if ($hook->url==$hook_url){
$hook_id = $hook->hook_id;
PodioHook::delete( $hook_id );
 }
}

PodioHook::create('app', '8327586', $attributes = array( 'type' => 'item.create', 'url' => $hook_url) );
PodioHook::create('app', '8327586', $attributes = array( 'type' => 'item.update', 'url' => $hook_url) );
PodioHook::create('app', '8327586', $attributes = array( 'type' => 'item.delete', 'url' => $hook_url) );

?>