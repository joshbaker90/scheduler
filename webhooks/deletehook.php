<?php
require_once(ABSPATH.'wp-config.php');
require_once(ABSPATH.'/wp-includes/wp-db.php');
require_once(ABSPATH.'wp-content/plugins/scheduler/podio/config.php');
require_once(ABSPATH.'wp-content/plugins/scheduler/podio/PodioAPI.php');
require_once(ABSPATH.'wp-content/plugins/scheduler/podio/authorise.php');

/*
  require_once '../../../../wp-config.php';
  require_once '../../../../wp-includes/wp-db.php';
  require_once '../podio/config.php';
  require_once '../podio/PodioAPI.php';
  require_once '../podio/authorise.php';
*/

$url = get_site_url();
$hookurl = '/wp-content/plugins/scheduler/webhooks/hook.php';

if (preg_match('/localhost/',$url)) {
$url1 = str_replace('localhost', '2.26.239.214', $url);
//str_replace('low', 'high', $oldurl);
$hook_url = $url1 . $hookurl;
}
else {
$hook_url = $url . $hookurl;
}

$hooks = PodioHook::get_for('app', 8327586 );

foreach ($hooks as $hook) {
  if ($hook->url==$hook_url){
$hook_id = $hook->hook_id;
PodioHook::delete( $hook_id );
 }
}


?>