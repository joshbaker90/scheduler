<?php

//get activity creation date
if(isset($item->created_on))
{ 
$ocreationdate = $item->created_on;
$creationdate = date_format($ocreationdate, 'y-m-d H:i:s');
$creationdate = date('Y-m-d H:i:s',strtotime($creationdate." UTC"))."\n";
}

//get activity published date
if(isset($item->fields['published-date']))
{
$publisheddate = $item->fields['published-date']->values['start'];
$published_date = $publisheddate->format('Y-m-d H:i:s');
}

if(isset($published_date))
{
$postdate = $published_date;
} else {
$postdate = $creationdate;
}

//get activity podio date
if(isset($item->last_event_on))
{
$olastpodio = $item->last_event_on;
$podiodate = date_format($olastpodio, 'y-m-d H:i:s');
$podiodate = date('Y-m-d H:i:s',strtotime($podiodate." UTC"))."\n";
$pmd = $podiodate;
}

//get activity activity
if(isset($item->fields['activity']->values))
{ 
$oactivity = $item->fields['activity']->values;
$activity = str_replace("'","(apos)","$oactivity");

}
else { $activity ="";
}

//get activity topic
if(isset($item->fields['topic']->values))
{ 
  //do something
$topics = ($item->fields['topic']->values);
foreach($topics as $topic) {
    //do something
$topicarray[] = $topic->title;
}
}
else {
}

//get activity goal
if(isset($item->fields['goal2']->values))
{ 
$ogoal = $item->fields['goal2']->values;
$goal = str_replace("'","(apos)","$ogoal");

}
else { $goal ="";
}

//get activity equipment
if(isset($item->fields['equipment-needed']->values))
{
$oequipment = $item->fields['equipment-needed']->values;
$equipment = str_replace("'","(apos)","$oequipment");
}
else { $equipment ="";
}

//get activity description
if(isset($item->fields['description-of-game']->values))
{ 
$description = $item->fields['description-of-game']->values;
$description = str_replace("'","(apos)","$description");
}
else { $description ="";
}

//get activity duration
if(isset($item->fields['duration']->values))
{
$duration = $item->fields['duration']->values/60;
$duration = round($duration, 2);
}
else { $duration ="";
}

//get activity links
if(isset($item->fields['link']->values))
{
$links = ($item->fields['link']->values);
foreach($links as $link) {
$linksarray[] = $link->resolved_url;
}
$urllinks = implode(',', $linksarray);
}
else { $urllinks ="";
}

//get activity images
if(isset($item->fields['image']->values))
{ 
 $images = ($item->fields['image']->values);
foreach($images as $image) {
$imagearray[] = '<a href="' .$image->link .'" target="_blank">'.$image->link.'</a>';
}
$urlimages = implode('<br>', $imagearray);
}
else { $urlimages ="";
}


//get publication status
$current_date = new DateTime();
$currentdate = strtotime($current_date->format('Y-m-d H:i:s'));
if(isset($publisheddate)){
$publisheddate = strtotime($publisheddate->date);
} else {
$publisheddate = 0;
}

if($publisheddate > $currentdate) {
$published = $item->fields['published']->values[0]['text'];
if ($published=='publish'){
$publish = 'draft';
}
else if ($published=='draft'){
$publish = 'draft';
}
else if ($published=='auto-draft'){
$publish = 'draft';
}
else if ($published=='trash'){
$publish = 'trash';
}
else if ($published=='pending'){
$publish = 'pending';
}
else if ($published=='future'){
$publish = 'future';
}
else if ($published=='private'){
$publish = 'private';
}
 else {
$publish = 'draft';	
}

} else {
if(isset($item->fields['published']->values[0]['text']))
{
$published = $item->fields['published']->values[0]['text'];

if ($published=='publish'){
$publish = 'publish';
}
else if ($published=='draft'){
$publish = 'draft';
}
else if ($published=='auto-draft'){
$publish = 'auto-draft';
}
else if ($published=='trash'){
$publish = 'trash';
}
else if ($published=='pending'){
$publish = 'pending';
}
else if ($published=='future'){
$publish = 'future';
}
else if ($published=='private'){
$publish = 'private';
}
 else {
$publish = 'draft';	
}
}
else {
$publish = 'draft';	
}
}

//get activity password
if(isset($item->fields['post-password']->values))
{
$post_password = $item->fields['post-password']->values;;
}
else
{
$post_password = '';
}

//$item = PodioItem::get( $podioid );
//get app_item_id (index no)
//$app_item_id = $item->app_item_id;

if(isset($item->fields['origin']->values))
{ 
$origin = strip_tags($item->fields['origin']->values);
//$origin = str_replace("'","(apos)","$oorigin");

}
else { $origin ="";
}

if(isset($item->fields['wppostid']->values))
{ 
$owppostid = $item->fields['wppostid']->values;
$wppostid = round(str_replace("'","(apos)","$owppostid"));

}
else { $wppostid ="";
}

?>