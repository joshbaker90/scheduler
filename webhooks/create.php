<?php

$sprefix = 'swp_';
global $wpdb;
$wpprefix = $wpdb->prefix;

  // Turn on debugging
Podio::$debug = true;

error_log("validate triggered");

$item = PodioItem::get( $podioid );
//get app_item_id (index no)
$app_item_id = $item->app_item_id;

if(isset($item->fields['sync']->values[0]['text']))
{
$sync = $item->fields['sync']->values[0]['text'];
}
else {
$sync = 'Yes'; 
}
if ($sync=='Yes'){

require 'fielddata.php';

$new_activitypost = array(
  'post_title'    => $activity,
  'post_content'  => $description,
  'post_status'    => $publish,
  'comment_status' => 'open',
  'ping_status'    => 'open',
  'post_name'      => $postname,
  'post_type'      => 'activities',
  'post_date'  => $postdate,
  'post_password' => $post_password
);
$postid = wp_insert_post($new_activitypost);

add_post_meta($postid, "$sprefix".'lastupdatedby', "podio", true);
add_post_meta($postid, "$sprefix".'podioid', $podioid, true);
add_post_meta($postid, "$sprefix".'goal', $goal, true);
add_post_meta($postid, "$sprefix".'equipment', $equipment, true);
add_post_meta($postid, "$sprefix".'duration', $duration, true);
add_post_meta($postid, "$sprefix".'link_url', $urllinks, true);


unset($new_activitypost);


//update the activity topic
global $wpdb;
$wpdb->query($wpdb->prepare("DELETE FROM ".$wpprefix."term_relationships WHERE object_id = %d",$postid));
if(isset($topicarray)) {
foreach ($topicarray as $topictitle) {

global $wpdb;
$ttid = $wpdb->get_results( "SELECT * FROM ".$wpprefix."terms WHERE name = '$topictitle'", OBJECT );
if(count($ttid) == 0)
{ // If topic isnt found...


$slug = str_replace(" ", "-", $topictitle);
global $wpdb;
$wpdb->insert(
  $wpprefix.'terms', 
  array( 
    'name' => $topictitle, 
    'slug' => $slug,
    'term_group' => 0
  ), 
  array(
    '%s', 
    '%s',
    '%d' 
  ) 
);

$ttid = $wpdb->get_results( "SELECT * FROM ".$wpprefix."terms WHERE name = '$topictitle'", OBJECT );
$termid = $ttid[0]->term_id;
}
else
  { //if topic is found...
$termid = $ttid[0]->term_id;
}

$ttaxonomy = $wpdb->get_results( "SELECT * FROM ".$wpprefix."term_taxonomy WHERE term_id = '$termid'", OBJECT );
if(count($ttaxonomy) == 0)
{ // If ttaxonomy isnt found...
global $wpdb;
$wpdb->insert( 
  $wpprefix.'term_taxonomy', 
  array( 
    'term_taxonomy_id' => $termid, 
    'term_id' => $termid,
    'taxonomy' => 'topics',
    'description' => 0,
    'parent' => 0,
    'count' => 1
    ), 
  array( 
    '%d',
    '%d', 
    '%s',
    '%d',
    '%d',
    '%d' 
  ) 
);
}


global $wpdb;
$wpdb->insert( 
  $wpprefix.'term_relationships', 
  array( 
    'object_ID' => $postid, 
    'term_taxonomy_id' => $termid
  )
);

}}
unset($topicarray);


global $wpdb;
$wpdb->update( "$wpprefix"."posts", array( 'post_modified' => $pmd), array( 'post_name' => $postname ), array( '%s' ) );
}


?>