<?php

  require_once '../../../../wp-config.php';
  require_once '../../../../wp-includes/wp-db.php';
  require_once '../podio/config.php';
  require_once '../podio/PodioAPI.php';
  require_once '../podio/authorise.php';


echo "<H1>Webhook Testing</H1><br><br>";

global $wpdb;
$wpprefix = $wpdb->prefix;

$item_id = '274052831';
$podioid = $item_id;
$postname = $item_id;
$sprefix = 'swp_';
$publish = 'yes';
$item = PodioItem::get( $item_id );
//get app_item_id (index no)
//$app_item_id = $item->app_item_id;

print $item->fields['post-password']->values;


/*
//get activity creation date
if(isset($item->created_on))
{ 
$ocreationdate = $item->created_on;
$creationdate = date_format($ocreationdate, 'y-m-d H:i:s');
$creationdate = date('Y-m-d H:i:s',strtotime($creationdate." UTC"))."\n";
}

//get activity published date
if(isset($item->fields['published-date']))
{
$publisheddate = $item->fields['published-date']->values['start'];
$published_date = $publisheddate->format('Y-m-d H:i:s');
}

if(isset($published_date))
{ 
$postdate = $published_date;
} else {
$postdate = $creationdate;
}

//get activity podio date
if(isset($item->last_event_on))
{
$olastpodio = $item->last_event_on;
$podiodate = date_format($olastpodio, 'y-m-d H:i:s');
$podiodate = date('Y-m-d H:i:s',strtotime($podiodate." UTC"))."\n";
$pmd = $podiodate;
}

//get activity activity
if(isset($item->fields['activity']->values))
{ 
$oactivity = $item->fields['activity']->values;
$activity = str_replace("'","(apos)","$oactivity");

}
else { $activity ="";
}

//get activity topic
if(isset($item->fields['topic']->values))
{ 
  //do something
$topics = ($item->fields['topic']->values);
foreach($topics as $topic) {
    //do something
$topicarray[] = $topic->title;
}
}
else {
}

//get activity goal
if(isset($item->fields['goal2']->values))
{ 
$ogoal = $item->fields['goal2']->values;
$goal = str_replace("'","(apos)","$ogoal");

}
else { $goal ="";
}

//get activity equipment
if(isset($item->fields['equipment-needed']->values))
{
$oequipment = $item->fields['equipment-needed']->values;
$equipment = str_replace("'","(apos)","$oequipment");
}
else { $equipment ="";
}

//get activity description
if(isset($item->fields['description-of-game']->values))
{ 
$description = $item->fields['description-of-game']->values;
$description = str_replace("'","(apos)","$description");
}
else { $description ="";
}

//get activity duration
if(isset($item->fields['duration']->values))
{
$duration = $item->fields['duration']->values/60;
$duration = round($duration, 2);
}
else { $duration ="";
}

//get activity links
if(isset($item->fields['link']->values))
{
$links = ($item->fields['link']->values);
foreach($links as $link) {
$linksarray[] = $link->resolved_url;
}
$urllinks = implode(',', $linksarray);
}
else { $urllinks ="";
}

//get activity images
if(isset($item->fields['image']->values))
{ 
 $images = ($item->fields['image']->values);
foreach($images as $image) {
$imagearray[] = '<a href="' .$image->link .'" target="_blank">'.$image->link.'</a>';
}
$urlimages = implode('<br>', $imagearray);
}
else { $urlimages ="";
}


//get publication status
$current_date = new DateTime();
$currentdate = strtotime($current_date->format('Y-m-d H:i:s'));
$publisheddate = strtotime($publisheddate->format('Y-m-d H:i:s'));

if($publisheddate > $currentdate) {
$published = $item->fields['published']->values[0]['text'];
if ($published=='publish'){
$publish = 'draft';
}
else if ($published=='draft'){
$publish = 'draft';
}
else if ($published=='auto-draft'){
$publish = 'draft';
}
else if ($published=='trash'){
$publish = 'trash';
}
else if ($published=='pending'){
$publish = 'pending';
}
else if ($published=='future'){
$publish = 'future';
}
else if ($published=='private'){
$publish = 'private';
}
 else {
$publish = 'draft';	
}

} else {
if(isset($item->fields['published']->values[0]['text']))
{
$published = $item->fields['published']->values[0]['text'];

if ($published=='publish'){
$publish = 'publish';
}
else if ($published=='draft'){
$publish = 'draft';
}
else if ($published=='auto-draft'){
$publish = 'auto-draft';
}
else if ($published=='trash'){
$publish = 'trash';
}
else if ($published=='pending'){
$publish = 'pending';
}
else if ($published=='future'){
$publish = 'future';
}
else if ($published=='private'){
$publish = 'private';
}
 else {
$publish = 'draft';	
}
}
else {
$publish = 'draft';	
}
}

//$item = PodioItem::get( $podioid );
//get app_item_id (index no)
//$app_item_id = $item->app_item_id;

if(isset($item->fields['origin']->values))
{ 
$origin = strip_tags($item->fields['origin']->values);
//$origin = str_replace("'","(apos)","$oorigin");

}
else { $origin ="";
}

if(isset($item->fields['wppostid']->values))
{ 
$owppostid = $item->fields['wppostid']->values;
$wppostid = round(str_replace("'","(apos)","$owppostid"));

}
else { $wppostid ="";
}

echo $publish;
*/




//print_r(getdate());


/*
//print_r($item);
if(isset($item->fields['modified-date']))
{
$omodifieddate = $item->fields['modified-date']->start_date;
$modifieddate= date_format($omodifieddate, 'y-m-d H:i:s');
$modifieddate = date('Y-m-d H:i:s',strtotime($modifieddate." UTC"))."\n";
} else { $modifieddate = 0; }

print $modifieddate;


require(ABSPATH."wp-content/plugins/scheduler/webhooks/fielddata.php");
//require(ABSPATH."wp-content/plugins/scheduler/webhooks/update.php");
//require(ABSPATH."wp-content/plugins/scheduler/webhooks/fielddata.php");

echo "<br><b>".$activity."</b><br><br>";

global $wpdb;
$query = $wpdb->get_results( "SELECT * FROM ".$wpprefix."postmeta WHERE meta_value = '$podioid'", OBJECT );
$postid = $query[0]->post_id;

$postdata = $wpdb->get_results( "SELECT * FROM ".$wpprefix."posts WHERE ID = '$postid'", OBJECT );
$postmodified = $postdata[0]->post_modified;
echo "podiodate: ".$podiodate."<br>";
echo "postdate: ".$postmodified."<br>";
echo "modifieddate: ".$modifieddate."<br><br>";
echo "<hr>";

echo "<b>".$activity."</b><br><br>";

$podiodate=strtotime($podiodate);
$postmodified=strtotime($postmodified);
$modifieddate=strtotime($modifieddate);
echo "podiodate: ".$podiodate."<br>";
echo "postdate: ".$postmodified."<br>";
echo "modifieddate: ".$modifieddate."<br><br>";
echo "<hr>";

if (($podiodate > $postmodified) && ($modifieddate == $postmodified)) {
echo "podio updated at source and podio date is newer than post date ";

//global $wpdb;
//$wpdb->update( $wpprefix.'posts', array( 'post_modified' => $pmd), array( 'ID' => $postid ), array( '%s' ) );

}
else if (($podiodate > $postmodified) && ($modifieddate != $postmodified)) {
echo "podio was updated by foreign source and podio date is newer than post date ";

}
else if(($podiodate < $postmodified) && ($modifieddate < $postmodified)) {
 echo "post date is newer than podio date";
}
else if(($podiodate == $postmodified) && ($modifieddate == $postmodified))  {
  echo "podio date is the same as post date";
}
else { echo "uknown error, possibly date formatting"; }

echo "<hr>";

require(ABSPATH."wp-content/plugins/scheduler/podiohooks/fielddata.php");
*/

?>