<?php
//phpinfo();

  require_once '../../../../wp-config.php';
  require_once '../../../../wp-includes/wp-db.php';
  require_once '../podio/config.php';
  require_once '../podio/PodioAPI.php';
  require_once '../podio/authorise.php';

$podioid = $_POST['item_id'];
$postname = $podioid;
global $wpdb;
$wpprefix = $wpdb->prefix;

switch ($_POST['type']) {
  case 'hook.verify':
    // Validate the webhook
    PodioHook::validate($_POST['hook_id'], array('code' => $_POST['code']));
  break;
  case 'item.create':
    # Do something. item_id is available in params['item_id']
  require_once 'update.php';
  break;
  case 'item.update':
    # Do something. item_id is available in params['item_id']
  require_once 'update.php';
  break;
  case 'item.delete':
    # Do something. item_id is available in params['item_id']
  require_once 'delete.php';
  break;
}

//UPDATE COUNT topic count//
global $wpdb;
$ttaxonomy= $wpdb->get_results( "SELECT * FROM ".$wpprefix."term_taxonomy WHERE taxonomy='topics'", OBJECT );

foreach ($ttaxonomy as $ttax) {
$termid = $ttax->term_id;

$tcount= $wpdb->get_results( "SELECT * FROM ".$wpprefix."term_relationships WHERE term_taxonomy_id='$termid'", OBJECT );
$termcount = count($tcount);

$wpdb->update( 
  $wpprefix.'term_taxonomy', 
  array( 
    'count' => $termcount,  // integer (number)
    
  ), 
  array( 'term_id' => $termid ), 
  array( 
    '%d'  // value1
  ), 
  array( '%d' ) 
);
}
?>